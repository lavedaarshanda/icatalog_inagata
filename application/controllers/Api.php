<?php
class Api extends My_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Login_m','login_m');
		$this->load->model('admin/Produk_m','produk_m');
		$this->load->model('admin/Banner_m','banner_m');
		$this->load->model('admin/Profil_m','profil_m');
	}
	public function generate_api($status=false,$msg='',$data=[])
	{
		$arr = [
			'error' => $status,
			'message' => $msg,
			'data' => $data
		];
		echo json_encode($arr);
	}
	public function pilih_toko()
	{
		$status = false;
		$msg = '';
		$arr = array();
		$toko = $this->input->post('toko');

		$token = $this->login_m->pilih_toko($toko);
		// echo $toko;
		// echo json_encode($token->row());
		// die();
		if ($token) {
			if ($token->num_rows()>0) {
				$arr = array(
					'token' => $token->row()->token
				);
			}else{
				$status = true;
				$msg = 'data kosong';
			}
		}else{
			$status = true;
			$msg = 'sql error';
		}
		echo $this->generate_api($status,$msg,$arr);
	}
	public function produk()
	{
		$status = false;
		$msg = '';
		$arr = array();
		$kate = $this->input->post('kategori');
		$param = $this->input->post('param');
		$token = $this->input->post('token');
		$id_produk = $this->input->post('id_produk');
		$data = $this->produk_m->get_produk($token,$param,$kate,$id_produk);
		if ($data) {
			foreach ($data as $key => $value) {
				if ($value->gambar1!='') {
					$value->gambar1 = base_url('/uploads/products/'.$value->username.'/'.$value->gambar1);		
				}
				$prdk = array(
					'id' => $value->id,
					'nama' => $value->nama,
					'id_kate' => $value->id_kate,
					'kategori' => $value->kategori,
					'gambar' => $value->gambar1,
				);
				array_push($arr, $prdk);
			}
			// $arr = $data;
		}else{
			$status = true;
			$msg = 'data kosong';
		}
		echo $this->generate_api($status,$msg,$arr);
	}
	public function detail_produk($id)
	{
		$status = false;
		$msg = '';
		$headers = apache_request_headers();
		$token = $headers['token'];
		$arr = array();
		$data = $this->produk_m->get_detail_produk($id,$token);
		$data->gambar = array();
		if ($data) {
			if ($data->gambar1!='') {
				array_push($data->gambar, base_url('/uploads/products/'.$data->username.'/'.$data->gambar1));
			}
			if ($data->gambar2!='') {
				array_push($data->gambar, base_url('/uploads/products/'.$data->username.'/'.$data->gambar2));	
			}
			if ($data->gambar3!='') {
				array_push($data->gambar, base_url('/uploads/products/'.$data->username.'/'.$data->gambar3));
			}	
			unset($data->gambar1);
			unset($data->gambar2);
			unset($data->gambar3);
			$arr = $data;
		}else{
			$status = true;
			$msg = 'data kosong';
		}
		echo $this->generate_api($status,$msg,$arr);
	}
	public function slider()
	{
		$status = false;
		$msg = '';
		$arr = array();
		$headers = apache_request_headers();
		$token = $headers['token'];
		$data = $this->banner_m->get_slider($token);
		if ($data) {
			foreach ($data as $key => $value) {
				$value->gambar = base_url('/uploads/banner/'.$value->username.'/'.$value->gambar);		
								
				array_push($arr, $value);
			}
		}else{
			$status = true;
			$msg = 'data kosong';
		}
		echo $this->generate_api($status,$msg,$arr);
		
	}
	public function profil()
	{
		$status = false;
		$msg = '';
		$arr = array();
		$headers = apache_request_headers();
		$token = $headers['token'];
		$data = $this->profil_m->get_profil_api($token);
		if ($data) {
			$data->logo = base_url('/uploads/logo/'.$data->username.'/'.$data->logo);		
			$data->banner = base_url('/uploads/banner_profil/'.$data->username.'/'.$data->banner);
			$arr=$data;
		}else{
			$status = true;
			$msg = 'data kosong';
		}
		echo $this->generate_api($status,$msg,$arr);
		
	}
}
?>