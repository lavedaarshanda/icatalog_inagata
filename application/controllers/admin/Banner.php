<?php
class Banner extends Operator_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Banner_m','banner_m');
        $this->load->helper(array('url','file'));
		$this->data = array(
			'halaman' => 'banner',
			'banner_count' => $this->banner_m->cekrow(),
			'banner' => $this->banner_m->get_banner()->result(),
			'produk' => $this->banner_m->get_produk()->result(),
		);
	}

	public function index()
	{
		$this->data['main_view'] = 'admin/banner_v';

		$this->load->view($this->data['main_view'], $this->data);
	}

	public function form()
	{
		date_default_timezone_set('Asia/Jakarta');
		$id = strftime("%Y%m%d%H%M%S",strtotime(date("Y-m-d H:i:s"))) . substr(microtime(),2,3);
		$nama = $this->input->post("nama");
		$id_produk = $this->input->post("id_produk");

            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "bannerpd_".$id;
            $config['upload_path'] = './uploads/banner/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);

            if ($this->upload->do_upload('gambar')){
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config['quality'] = 100;
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 600;

                $this->load->library('image_lib', $config);

                if(!$this->image_lib->resize()){
                    echo $this->image_lib->display_errors();
                }

                $data_banner = $this->upload->data();

                $newdata['gambar'] = $data_banner['file_name'];
            }else{
                $this->session->set_flashdata('message', $this->upload->display_errors('', ''));
            }
            //end of file upload codes

            $newdata = array(
                'id'=>$id,
                'id_akun'=>$this->session->userdata('id_akun'),
                'nama'=>$nama,
                'gambar'=>$data_banner['file_name'],
                'id_produk'=>$id_produk,
            );       
                
            $this->db->insert('tb_banner', $newdata); 

            redirect(base_url()."admin/banner");
	}

	public function del($id)
	{
		$img = $this->banner_m->uimg($id)->row()->gambar;

		if($this->banner_m->delete($id) == TRUE)
		{
			@unlink('./uploads/banner/'.$this->session->userdata('username').'/'.$img);
		}

		redirect(base_url()."admin/banner");
	}
}
?>