<?php
class Editproduk extends Operator_Controller
{
	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Produk_m', 'produk');
        $this->load->helper(array('url', 'form'));
		$this->data = array(
			'halaman' => 'tambahproduk',
        	'main_view' => 'admin/editproduk_v',
			'kategori' => $this->produk->get_kategori(),
        );
	}

	public function index()
	{
		$this->data['form_action'] = site_url('admin/editproduk');
        $this->load->view($this->layout, $this->data);
	}

    public function edit($id)
    {
        $this->load->view($this->layout, $this->data);
        $this->formedit($id);
    }

	public function formedit($id){

            $nama = $this->input->post("nama");
            $kategori = $this->input->post("kategori");
            $deskripsi = $this->input->post("deskripsi");
            $link = $this->input->post("link");

						$url = substr($link,0,4);

						if($url == "http"){
		        	$newdata = array(
		            'nama'=>$nama,
		            'id_kate'=>$kategori,
		            'deskripsi'=>$deskripsi,
		            'link'=>$link,
		        	);
						}else{
							$newdata = array(
		            'nama'=>$nama,
		            'id_kate'=>$kategori,
		            'deskripsi'=>$deskripsi,
		            'link'=>'http://'.$link,
		        	);
						}

            //this code is for the file upload
            if($_FILES['gambar1']['name']){
                $this->load->library('upload');
                $nmfile = "product_".$this->session->userdata('username').'_'.$nama;
                $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
                $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                $config['overwrite'] = TRUE;
                $config['max_size'] = '4000';
                $config['file_name'] = $nmfile;
                $this->upload->initialize($config);

                if($this->upload->do_upload('gambar1'))
                {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config['quality'] = 100;
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 300;
                    $config['height'] = 300;

                    $this->load->library('image_lib', $config);

                    if(!$this->image_lib->resize()){
                        echo $this->image_lib->display_errors();
                    }

                    $data = $this->upload->data();

                    $newdata['gambar1'] = $data['file_name'];
                }else{
                    $this->session->set_flashdata('message', $this->upload->display_errors('', ''));
                }
                //end of file upload codes

            }

            if($_FILES['gambar2']['name']){
                $this->load->library('upload');
                $nmfile = "file_".$nama."_2";
                $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
                $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                $config['overwrite'] = TRUE;
                $config['max_size'] = '4000';
                $config['file_name'] = $nmfile;
                $this->upload->initialize($config);
                $this->upload->do_upload('gambar2');
                $data = $this->upload->data();
                //end of file upload codes

                $newdata['gambar2'] = $data['file_name'];
            }

            if($_FILES['gambar3']['name']){
                $this->load->library('upload');
                $nmfile = "file_".$nama."_3";
                $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
                $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                $config['overwrite'] = TRUE;
                $config['max_size'] = '4000';
                $config['file_name'] = $nmfile;
                $this->upload->initialize($config);
                $this->upload->do_upload('gambar3');
                $data = $this->upload->data();
                //end of file upload codes

                $newdata['gambar3'] = $data['file_name'];
            }

            $this->db->where('id',$id);
            $this->db->update('tb_produk', $newdata);
            redirect(base_url()."admin/produk");

	}

}
?>
