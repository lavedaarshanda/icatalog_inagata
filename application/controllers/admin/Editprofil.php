<?php
class Editprofil extends Operator_Controller
{
	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Profil_m', 'profil');
        $this->load->helper(array('form', 'url'));
		$this->data = array(
			'halaman' => 'editprofil',
        	'main_view' => 'admin/editprofil_v',
			'profils' => $this->profil->get_profil(),
		);
	}

	public function index()
	{
		$this->data['form_action'] = site_url('admin/editprofil');
        $this->load->view($this->data['main_view'], $this->data);
	}
	
	public function sukses()
    {
        $this->data['main_view'] = 'sukses';
        $this->data['title'] = 'Edit Profil Sukses';
        $this->load->view($this->data['main_view'], $this->data);
    }

    public function error()
    {
        $this->data['main_view'] = 'error';
        $this->data['title'] = 'Edit Profil Error';
        $this->load->view($this->data['main_view'], $this->data);
    }

	public function form(){
        $nama = $this->input->post("nama_perusahaan");
        $id_akun = $this->session->userdata("id_akun");
        $alamat = $this->input->post("alamat");
        $telp = $this->input->post("telp");
        $email = $this->input->post("email");
        $ig = $this->input->post("instagram");
        $fb = $this->input->post("facebook");
        $yt = $this->input->post("youtube");
        $deskripsi = $this->input->post("deskripsi");
        $visi = $this->input->post("visi");
        $misi = $this->input->post("misi");

        $newdata = array(
                'nama_perusahaan'=>$nama,
                'id_akun'=>$id_akun,
                'alamat'=>$alamat,
                'telp'=>$telp,
                'email'=>$email,
                'instagram'=>$ig,
                'facebook'=>$fb,
                'youtube'=>$yt,
                'deskripsi'=>$deskripsi,
                'visi'=>$visi,
                'misi'=>$misi,
        );

        if(!empty($_FILES['logo']['name'])){

            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "logo_".$this->session->userdata('username');
            $config['upload_path'] = './uploads/logo/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['overwrite'] = TRUE;
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);

            if ($this->upload->do_upload('logo')){
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config['quality'] = 100;
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;

                $this->load->library('image_lib', $config);

                if(!$this->image_lib->resize()){
                    echo $this->image_lib->display_errors();
                }

                $data_logo = $this->upload->data();

                $newdata['logo'] = $data_logo['file_name'];
            }else{
                $this->session->set_flashdata('message_err', $this->upload->display_errors('', ''));
            }
            //end of file upload codes

        }



        if(!empty($_FILES['banner']['name'])){
            
            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "bannerpf_".$this->session->userdata('username');;
            $config['upload_path'] = './uploads/banner_profil/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['overwrite'] = TRUE;
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);

            if ($this->upload->do_upload('banner')){
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config['quality'] = 100;
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 400;
                $config['height'] = 400;

                $this->load->library('image_lib', $config);

                if(!$this->image_lib->resize()){
                    echo $this->image_lib->display_errors();
                }

                $data_banner = $this->upload->data();

                $newdata['banner'] = $data_banner['file_name'];
            }else{
                $this->session->set_flashdata('message_err', $this->upload->display_errors('', ''));
            }
            //end of file upload codes

        }

        $this->db->where('id_akun',$id_akun);
        $this->db->update('tb_profil', $newdata);
        redirect(base_url()."admin/editprofil");
	}

    public function akun()
    {
        $this->data['main_view'] = 'admin/akun_v';
        $this->data['akun'] = $this->profil->get_akun();

        $this->data['form_action'] = site_url('admin/editprofil/cek_validasi');
        $this->load->view($this->data['main_view'], $this->data);
    }

    public function cek_validasi()
    {
        $old_pw = $this->input->post('old_pass');
        $new_pw = $this->input->post('new_pass');
        $pw = $this->input->post('password');

        $md5 = md5($this->input->post('password'));

        if ($this->profil->validate('form_rules_pwd')) {
            $old_pwd = $this->profil->getoldpwd();
            $old_pwd_in = md5($this->input->post('old_pass'));
            $akun_post = array(
                'password' => md5($this->input->post('password')),
            );
            
            if($old_pwd_in == $old_pwd) {
                if (!$this->profil->simpan_pwd($akun_post)) {
                    $this->session->set_flashdata('status_pwd', '1');
                    redirect(base_url('admin/editprofil'));
                } else {
                    $this->session->set_flashdata('status_pwd', '2');
                    redirect(base_url('admin/editprofil'));
                }
            }else{
                    $this->session->set_flashdata('status_pwd', '3');
                    redirect('admin/editprofil/akun');
            }
        }
        if($old_pw == null || $new_pw == null || $pw == null)
        {
            $this->session->set_flashdata('status_pwd', '5');
            redirect('admin/editprofil/akun');
        }

        $this->session->set_flashdata('status_pwd', '4');
        redirect(base_url('admin/editprofil/akun'));
    }

    public function update_akun()
    {
        $uname = $this->input->post('username');
        $old_pwd = $this->input->post('old_pass');
        $new_pwd = $this->input->post('new_pass');
        $k_new_pwd = $this->input->post('password');


        $this->data['akun'] = $this->profil->get_akun();
        $this->load->view(data['main_view'], $this->data);
    }
}
?>