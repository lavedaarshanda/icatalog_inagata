<?php 
class Forget extends CI_Controller
{

	public function index()
	{
    if($this->session->userdata('login_status') == TRUE)
    {
      redirect('admin/home');
    }else{
      $this->data['main_view'] = 'admin/account_v';

      $this->load->view($this->data['main_view'], $this->data);
    }
	}

  public function verification()
  {
    $email = $this->input->post('email');

    $is_valid_email = $this->check_email($email);

    if($is_valid_email == TRUE){

      $this->db->select('id_akun');
      $this->db->where('email', $email);
      
      $id = $this->db->get('tb_profil')->row()->id_akun;

      $this->db->select('token');
      $this->db->where('id_akun', $id);
      
      $token = $this->db->get('tb_akun')->row()->token;

      if($this->send_email($email, $token)){

        $this->session->set_flashdata('ttlfrg', "Email Berhasil Dikirim");
        $this->session->set_flashdata('msgfrg', 'Link verifikasi berhasil dikirim. Silahkan cek kotak masuk email anda.');

        redirect('admin/forget/message');
      }else{

        $this->session->set_flashdata('ttlfrg', "Gagal Mengirim Email");
        $this->session->set_flashdata('msgfrg', 'Terjadi kesalahan ketika mengirim link ke email anda. Silahkan coba lagi beberapa saat nanti.');

        redirect('admin/forget/message');
      }

    }else{

      $this->session->set_flashdata('ttlfrg', "Email Tidak Ditemukan");
      $this->session->set_flashdata('msgfrg', 'Maaf, email yang anda masukkan tidak terdaftar di akun manapun. Jika terjadi kesalahan, hubungi: <a href="mailto:contact@inagata.com">contact@inagata.com</a>.');

      redirect('admin/forget/message');
    }

  }

  function check_email($email)
  { 
    $this->db->select('email');
    $this->db->where('email', $email);
    $num = $this->db->get('tb_profil')->num_rows();

    if($num > 0){

      return true;

    }else{

      return false;

    }

  }

	function send_email($email, $token) 
	{
    $this->load->library('email');
    $config = Array(
      'charset' => 'utf-8',
      'mailtype' => 'html',
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.gmail.com',
      'smtp_port' => 465,
      'smtp_timeout' => 400,
      'smtp_user' => 'blewtechnologies@gmail.com',
      'smtp_pass' => 'blewtech',
      'crlf' => "\r\n",
      'newline' => "\r\n",
      'wordwrap' => TRUE
    );

    $this->email->initialize($config);

    $this->email->from($config['smtp_user']);
    $this->email->to($email);
    $this->email->subject("Verifikasi Lupa Kata Sandi iCatalog");
    $this->email->message(
     "Anda telah meminta untuk reset kata sandi. Untuk memverifikasi, silahkan klik link dibawah ini:<br><br>".site_url("admin/forget/verify/".$token)."<br><br>Jika anda tidak merasa melakukan ini, silahkan abaikan pesan ini.<br><br><br>iCatalog Team"
    );

    return $this->email->send();
  }

	public function verify($token)
  {
    if($this->session->userdata('login_status') == TRUE)
    {
      redirect('admin/home');
    }else{
      $this->load->helper('url');

      $this->db->where('token', $token);
      $q = $this->db->get('tb_akun');
    
      if($q->num_rows() > 0){
        $this->data['id_akun_reset'] = $q->row()->id_akun;

        $this->data['main_view'] = 'admin/resetpwd_v';
        $this->load->view($this->data['main_view'], $this->data);
      }else{
        $this->session->set_flashdata('ttlfrg', "ERROR 403");
        $this->session->set_flashdata('msgfrg', "FORBIDDEN ACCESS!");

        redirect('admin/forget/message');
      }
    }
  }

  function resetpwd($id)
  {
    $pwd1 = $this->input->post('password');

    $arr = array(
      'password' => md5($pwd1),
    );

    $this->db->where('id_akun', $id);
    
    if($this->db->update('tb_akun', $arr)){
      $this->session->set_flashdata('msgfrg', 'Password anda berhasil dirubah. Silahkan kembali ke halaman Login.');
      
      redirect('admin/forget/message');
    }else{
      $this->session->set_flashdata('msgrst', 'Terjadi kesalahan ketika mengganti password. Silahkan coba lagi beberapa saat lagi.');

      $this->data['main_view'] = 'admin/resetpwd_v';
      $this->load->view($this->data['main_view'], $this->data);
    }
  }

  public function message()
  { 
    if($this->session->userdata('login_status') == TRUE)
    {
      redirect('admin/home');
    }else{
      $this->data['main_view'] = 'admin/forget_msg_v';

      $this->load->view($this->data['main_view'], $this->data);
    }
  }
}
?>