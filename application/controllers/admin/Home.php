<?php
class Home extends Operator_Controller
{
    public $data = array(
        'halaman' => 'home',
        'main_view' => 'admin/home_v'
    );

	public function __construct()
    {
        parent::__construct();
		
    }
	
    public function index()
    {
        $this->load->view($this->data['main_view'], $this->data);
    }
}