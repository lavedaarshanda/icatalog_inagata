<?php
class Kategori extends Operator_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/Kategori_m','kategori_m');
        $this->load->helper('url');
		$this->data = array(
			'halaman' => 'kategori',
			'kategori' => $this->kategori_m->get_kategori()->result(),
			'jml_kategori' => $this->kategori_m->get_kategori()->num_rows(),
		);

    }
    
    public function index()
    {
		$this->data['main_view'] = 'admin/kategori_v';

		$this->load->view($this->data['main_view'],$this->data);
    }
	
	public function hapus($id)
	{
		$this->db->where('id_kate', $id);
		$this->db->update('tb_produk', array('id_kate'=>'1'));
		
		$this->kategori_m->hapusdata($id);
		redirect('admin/kategori');
	}

	public function edit($id)
	{
		$where = array('id' => $id);

		$this->data['kategori'] = $this->kategori_m->update_k($where, 'tb_kategori')->result();
		$this->data['main_view'] = 'admin/editkategori_v';

		$this->load->view($this->layout,$this->data);
    }

    public function update($id,$kateg)
    { 
		$data = array(
			'kategori' => urldecode($kateg),
		);
 
		$where = array(
			'id' => $id,
		);
 
		$this->kategori_m->update_data($where,$data,'tb_kategori');
		redirect('admin/kategori');
    }

    public function add($kateg)
    { 
		$data = array(
			'id_akun' => $this->session->userdata('id_akun'),
			'kategori' => urldecode($kateg),
		);
 
		$this->kategori_m->set_data($data,'tb_kategori');
		redirect('admin/kategori');
    }

}