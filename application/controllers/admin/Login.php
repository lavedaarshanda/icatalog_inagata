<?php
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Login_m', 'login');
		$this->load->library('session');
    }

    public function index()
    {
		if($this->session->userdata('login_status') == true)
		{
			$this->session->set_flashdata('welcome', '1');
			redirect('admin/home');
		}
			// Validasi.
			if (! $this->login->validate('form_rules')) {
				$data['validation_errors'] = $this->form_validation->error_array();
				$this->load->view('admin/login_v', $data);
				return;
			}

			// Login
			//$login = $this->input->post(null, true);
			$login = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
			);
			if (! $this->login->login($login)) {
				$this->session->set_flashdata('pesan_error', 'Username atau Password salah. Silahkan coba lagi.');
				redirect('admin/login-error');
			}
		
		
		redirect('admin/home');
    }

    public function error()
    {
        $this->load->view('admin/login_v');
    }

    public function logout()
    {
        $this->login->logout();
        redirect('admin/home');
    }
}