<?php
class Produk extends Operator_Controller
{
   public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/Produk_m','produk_m');
        $this->load->helper(array('url', 'file'));
		$this->data = array(
			'halaman' => 'produk',
        	'main_view' => 'admin/produk_v',
			'produk' => $this->produk_m->getAll()->result(),
			'jml_prod' => $this->produk_m->getAll()->num_rows(),
            'kategori' => $this->produk_m->get_kategori(),
		);

    }

    public function index()
    {
        $this->data['main_view'] = 'admin/produk_v';

        $this->load->view($this->data['main_view'], $this->data);
    }

    public function hapus($id)
	{
        $this->db->where('id', $id);
        $nm_prod = $this->db->get('tb_produk')->row();

		if($this->produk_m->hapusdata($id) == TRUE)
        {
            if($nm_prod->gambar1)
            {
                $file1 = './uploads/products/'.$this->session->userdata('username').'/'.$nm_prod->gambar1;

                @unlink($file1);
            }

            if($nm_prod->gambar2)
            {
                $file2 = './uploads/products/'.$this->session->userdata('username').'/'.$nm_prod->gambar2;

                @unlink($file2);
            }

            if($nm_prod->gambar3)
            {
                $file3 = './uploads/products/'.$this->session->userdata('username').'/'.$nm_prod->gambar3;

                @unlink($file3);
            }

            echo "db deleted!";
        }

		redirect('admin/produk');
	}

    public function edit($id)
    {
        $where = array('id' => $id);

        $this->data['produk'] = $this->produk_m->update_p($where, 'tb_produk')->result();

        $this->data['prod'] = $this->produk_m->getSelected($id);

        $this->data['main_view'] = 'admin/editproduk_v';

        $this->load->view($this->data['main_view'],$this->data);
    }

    public function details($id)
    {
        $this->data['gbr'] = $this->produk_m->get_img_list($id)->row();
        $this->data['main_view'] = 'admin/detail_v';
        $this->data['detail'] = $this->produk_m->get_detail($id);
        $this->load->view($this->data['main_view'], $this->data);
    }

    public function update()
    {
        $xname = $this->data['detail']['nama'];
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $id_kate = $this->input->post('id_kate');
        $deskripsi = $this->input->post('deskripsi');
        $link = $this->input->post('link');

        $url = substr($link,0,4);

				if($url == "http"){
        	$newdata = array(
            'nama'=>$nama,
            'id_kate'=>$id_kate,
            'deskripsi'=>$deskripsi,
            'link'=>$link,
        	);
				}else{
					$newdata = array(
            'nama'=>$nama,
            'id_kate'=>$id_kate,
            'deskripsi'=>$deskripsi,
            'link'=>'http://'.$link,
        	);
				}

        if($_FILES['gambar1']['name'])
        {
            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "file_".$nama."_1";
            $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['overwrite'] = TRUE;
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);
            $this->upload->do_upload('gambar1');
            $data = $this->upload->data();
            //end of file upload codes

            $newdata['gambar1'] = $data['file_name'];
        }

        if($_FILES['gambar2']['name'])
        {
            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "file_".$nama."_2";
            $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['overwrite'] = TRUE;
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);
            $this->upload->do_upload('gambar2');
            $data = $this->upload->data();
            //end of file upload codes

            $newdata['gambar2'] = $data['file_name'];
        }

        if($_FILES['gambar3']['name'])
        {
            //this code is for the file upload
            $this->load->library('upload');
            $nmfile = "file_".$nama."_3";
            $config['upload_path'] = './uploads/products/'.$this->session->userdata('username').'/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
            $config['overwrite'] = TRUE;
            $config['max_size'] = '4000';
            $config['file_name'] = $nmfile;
            $this->upload->initialize($config);
            $this->upload->do_upload('gambar3');
            $data = $this->upload->data();
            //end of file upload codes

            $newdata['gambar3'] = $data['file_name'];
        }

        $where = array(
            'id' => $id
        );

        $this->produk_m->update_data($where,$newdata,'tb_produk');
        redirect('admin/produk');
        echo $newdata['gambar1']." & ".$newdata['gambar2']." & ".$newdata['gambar3'];

    }

    public function search($q)
    {
        $this->data['produk'] = $this->produk_m->srch($q)->result();

        $this->data['main_view'] = 'admin/produk_v';

        $this->load->view($this->data['main_view'], $this->data);
    }
}
