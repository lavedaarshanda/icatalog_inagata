<?php
class Profil extends Operator_Controller
{
	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Profil_m','profil_m');
        $this->load->helper(array('url'));
		$this->data = array(
			'halaman' => 'profil',
        	'main_view' => 'admin/profil_v',
			'profil' => $this->profil_m->get_profil(),
		);
	}
		
    public function index()
    {				
		//$status = $this->session->flashdata('status');
		//echo $status;
        $this->load->view($this->data['main_view'], $this->data);
    }
}
?>