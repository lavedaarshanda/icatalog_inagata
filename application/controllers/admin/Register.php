<?php
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Register_m','register_m');
        $this->data['main_view'] = 'admin/register_v';
    }

    public function index()
    {
		if($this->session->userdata('login_status') == true)
		{
			redirect('admin/home');
		}
			
		$this->load->view($this->data['main_view']);
		
    }

    public function reg()
    {
    	$akun = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
		);

		$profil = array(
			'nama_perusahaan' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'telp' => $this->input->post('no_telp'),
		);

		$this->db->where('username', $akun['username']);
		$q = $this->db->get('tb_akun');

		if($q->num_rows() > 0){
			$this->session->set_flashdata('regmsg', 'Username telah terdaftar.');

			redirect('admin/register');
		}else{
			$this->db->where('email', $profil['email']);
			$query = $this->db->get('tb_profil');

			if($query->num_rows() > 0){
				$this->session->set_flashdata('regmsg', 'Email telah terdaftar.');

				redirect('admin/register');
			}else{

				$this->register_m->register($akun, $profil);

				$logo = './uploads/logo/'.$akun['username'];
				$produk = './uploads/products/'.$akun['username'];
				$banner = './uploads/banner/'.$akun['username'];
				$banner_profil = './uploads/banner_profil/'.$akun['username'];

				if(!is_dir($logo))
				{
					mkdir($logo, 0755, true);
				}

				if(!is_dir($produk))
				{
					mkdir($produk, 0755, true);
				}

				if(!is_dir($banner))
				{
					mkdir($banner, 0755, true);
				}

				if(!is_dir($banner_profil))
				{
					mkdir($banner_profil, 0755, true);
				}

				redirect('admin/login');

			}
		}

    }

}