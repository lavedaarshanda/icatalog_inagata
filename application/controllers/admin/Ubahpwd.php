<?php
class Ubahpwd extends Operator_Controller
{
	public function __construct() {
        parent::__construct();
		$this->load->model('admin/Ubahpwd_m','ubahpwd_m');
        $this->load->helper(array('url'));
		$this->data = array(
        	'main_view' => 'admin/ubahpwd_v',
			//'profil' => $this->profil_m->get_private(),
		);
	}

	public function index()
	{
		$this->load->view($this->data['main_view'], $this->data);
	}

	public function form()
	{
		$old_pwd = $this->input->post('old_pwd');
		$new_pwd = $this->input->post('new_pwd');
		$con_pwd = $this->input->post('con_pwd');
		$id = $this->session->userdata('id_akun');

		$where = "id_akun = ".$id;

		$table = "tb_akun";

		$is_true_old = $this->ubahpwd_m->check_old(md5($old_pwd), $id);

		if($is_true_old == 0 || $is_true_old == null){
			$this->session->set_flashdata('error', '100');
			redirect('admin/ubahpwd');
		}elseif($new_pwd != $con_pwd){
			$this->session->set_flashdata('error', '200');
			redirect('admin/ubahpwd');
		}else{
			$data = array(
				'password' => md5($new_pwd),
			);

			$this->ubahpwd_m->change_pwd($data, $where, $table);

			$this->session->set_flashdata('success', '120');
			redirect('admin/ubahpwd');
		}

	}
}
?>