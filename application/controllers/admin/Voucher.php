<?php
class Voucher extends Operator_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/Voucher_m','voucher_m');
        $this->load->helper('url');
		$this->data = array(
			'halaman' => 'voucher',
			'voucher' => $this->voucher_m->get_voucher()->result(),
			'jml_voucher' => $this->voucher_m->get_voucher()->num_rows(),
		);

    }
    
    public function index()
    {
		$this->data['main_view'] = 'admin/voucher_v';

		$this->load->view($this->data['main_view'],$this->data);
    }
	
	public function hapus($id)
	{
		$this->db->where('id_voucher', $id);
		$this->db->update('tb_produk', array('id_voucher'=>'1'));
		
		$this->voucher_m->hapusdata($id);
		redirect('admin/voucher');
	}

	public function edit($id)
	{
		$where = array('id' => $id);

		$this->data['voucher'] = $this->voucher_m->update_k($where, 'tb_voucher')->result();
		$this->data['main_view'] = 'admin/editvoucher_v';

		$this->load->view($this->layout,$this->data);
    }

    public function update($id,$voucher)
    { 
		$data = array(
			'voucher' => urldecode($voucher),
		);
 
		$where = array(
			'id' => $id,
		);
 
		$this->voucher_m->update_data($where,$data,'tb_voucher');
		redirect('admin/voucher');
    }

    public function add($voucher)
    { 
		$data = array(
			'id_akun' => $this->session->userdata('id_akun'),
			'voucher' => urldecode($voucher),
		);
 
		$this->voucher_m->set_data($data,'tb_voucher');
		redirect('admin/voucher');
    }

}