<?php
class Shome extends Operator_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Sadmin_m','sadmin_m');
				$this->data['main_view'] = 'superadmin/shome_v';
        $this->data['users'] = $this->sadmin_m->getallusr()->result();
        $this->data['users_count'] = $this->sadmin_m->getallusr()->num_rows();
    }

    public function index()
    {
        $this->load->view($this->data['main_view'], $this->data);
    }
}
