<?php
class Banner_m extends MY_Model {
	
	public function cekrow()
	{
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
		return $this->db->get('tb_banner')->num_rows();
	}

	public function get_banner()
	{
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
        return $this->db->get('tb_banner');
	}

	public function get_produk()
	{
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
		return $this->db->get('tb_produk');
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tb_banner');
	}

	public function uimg($id)
	{
		$this->db->select('gambar');
		$this->db->where('id', $id);
		return $this->db->get('tb_banner');
	}
	public function get_slider($token)
	{
        $this->db->select('b.*,p.nama as nama_produk,a.token,a.username');

		$this->db->where('a.token',$token);
		$this->db->from('tb_banner b');
		$this->db->join('tb_akun a','b.id_akun=a.id_akun');
		$this->db->join('tb_produk p','p.id=b.id_produk');
		return $this->db->get()->result();
	}
}
?>