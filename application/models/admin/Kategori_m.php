<?php 
class Kategori_m extends MY_Model
{	
	public function simpan($kategori)
    {
        $kategori = (object)$kategori;

        return $this->db->insert('tb_kategori', $kategori);
    }

    public function update_k($where, $table)
    {
        
        return $this->db->get_where($table, $where);
    }

     public function update_data($where,$data,$table)
    {
        $this->db->where($where);
		$this->db->update($table,$data);
    }

    public function set_data($data,$table)
    {
		$this->db->insert($table,$data);
    }
	
	public function get_kategori()
	{
		$this->db->where('id > 1');
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
		return $this->db->get('tb_kategori');
	}

	public function get_kategori2($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('tb_kategori');
	}
	
	public function hapusdata($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tb_kategori');
	}

}
?>