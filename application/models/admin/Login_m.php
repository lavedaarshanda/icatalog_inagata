<?php
class Login_m extends MY_Model
{
    public $_tabel = 'tb_akun';

    public $form_rules = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|xss_clean|required'
        ),
    );
	

    public function login($login)
    {
        $login = (object) $login;

		$this->db->select(
			'tb_akun.token,
            tb_akun.id_akun, 
            tb_akun.username,
            tb_profil.nama_perusahaan'
		);
		
		$this->db->from('tb_akun');

		$this->db->join('tb_profil', 'tb_akun.id_akun=tb_profil.id_akun');

		$where = "username = '$login->username' and password = '$login->password'";
		
		$this->db->where($where);

        $user = $this->db->get()->row();
		
        if ($user) {
		
            $data_session = array(
                'id_akun' => $user->id_akun,
                'login_status' => true,
				'username' => $user->username,
				'name' => $user->nama_perusahaan,
                'token' => $user->token,
            );
		
			$this->session->set_userdata($data_session);

            return true;
        }

        return false;
    }
    public function pilih_toko($toko)
    {
        $query = "Select a.token from tb_akun a , tb_profil p where a.id_akun = p.id_akun and (p.nama_perusahaan like '%".$toko."%' or a.username like '%".$toko."%');";
        return $this->db->query($query);
    }
    public function logout()
    {
        $this->session->unset_userdata(
            array(
				'id_akun' => '',
                'login_status' => false,
				'username' => '',
				'name' => '',
            )
        );
        $this->session->sess_destroy();
    }
}