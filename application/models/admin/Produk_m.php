<?php 
class Produk_m extends MY_Model
{
	public function getAll()
	{
		$this->db->select(
			'tb_produk.id, 
            tb_produk.nama, 
            tb_kategori.kategori,
            tb_produk.deskripsi,
            tb_produk.link,
            tb_produk.gambar1'

		);
		
		$this->db->from('tb_produk');

		$this->db->join('tb_kategori', 'tb_produk.id_kate=tb_kategori.id');

        $this->db->where(array('tb_produk.id_akun' => $this->session->userdata('id_akun')));

        return $this->db->get();
	}

	public function simpan($produk)
    {
        $produk = (object)$produk;

        $this->db->insert('tb_produk', $produk);
    }

	public function hapusdata($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tb_produk');
	}

	public function getSelected($data)
	{
		$this->db->where('id', $data);
		return $this->db->get('tb_produk')->row();
	}

	public function get_kategori()
	{
        $this->db->where('id > 1');
        $this->db->where('id_akun', $this->session->userdata('id_akun'));
		$query = $this->db->get('tb_kategori');
		return $query->result();
	}

    public function get_detail($id)
    {
        $this->db->select(
            'tb_produk.id, 
            tb_produk.nama, 
            tb_produk.deskripsi,
            tb_produk.link,
            tb_produk.gambar1,
            tb_produk.gambar2,
            tb_produk.gambar3,
            tb_kategori.kategori'
        );
        
        $this->db->from('tb_produk');
        $this->db->join('tb_kategori', 'tb_produk.id_kate=tb_kategori.id');
        $this->db->where(array('tb_produk.id' => $id));
        return $this->db->get()->row();
    }

	public function update_p($where, $table)
	{
        return $this->db->get_where($table, $where);
	}

	 public function update_data($where,$data,$table)
    {
        $this->db->where($where);
		$this->db->update($table,$data);
    }

	function insert($data){

		$this->db->insert('tb_produk', $data);
    }

    public function srch($q)
    {
        $this->db->select(
            'tb_produk.id, 
            tb_produk.nama, 
            tb_kategori.kategori,
            tb_produk.deskripsi,
            tb_produk.link,
            tb_produk.gambar1'

        );
        
        $this->db->from('tb_produk');

        $this->db->join('tb_kategori', 'tb_produk.id_kate=tb_kategori.id');

        $this->db->like('nama', $q);

        return $this->db->get();
    }

    public function get_img_list($id)
    {
        $this->db->select(
            'gambar1,
            gambar2,
            gambar3'
        );
        $this->db->where('id', $id);
        return $this->db->get('tb_produk');
    }
    public function get_produk($token,$param='',$kategori='')
    {
        $this->db->select('p.*,k.kategori,a.token,a.username');
        $this->db->from('tb_produk p');
        $this->db->join('tb_kategori k','k.id = p.id_kate');
        $this->db->join('tb_akun a','a.id_akun = p.id_akun');
        
        $this->db->where('a.token',$token);
        
        if ($param != '') {
            $this->db->like('p.nama',$param);
        }
        if ($kategori != '') {
            $this->db->where('p.id_kate',$kategori);
        }
        return $this->db->get()->result();
    }
    public function get_detail_produk($id,$token)
    {
        $this->db->select(
            'tb_produk.id, 
            tb_produk.nama, 
            tb_produk.deskripsi,
            tb_produk.link,
            tb_produk.gambar1,
            tb_produk.gambar2,
            tb_produk.gambar3,
            tb_akun.username'
        );
        
        $this->db->from('tb_produk');
        $this->db->join('tb_kategori', 'tb_produk.id_kate=tb_kategori.id');
        $this->db->join('tb_akun', 'tb_produk.id_akun=tb_akun.id_akun');
        $this->db->where(array('tb_produk.id' => $id,'tb_akun.token'=>$token));
        return $this->db->get()->row();
    }
}
?>