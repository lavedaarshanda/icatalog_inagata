<?php
class Profil_m extends MY_Model
{
	public function simpan($profil)
    {
        $profil = (object)$profil;
		
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
        return $this->db->update('tb_profil', $profil);
    }
	
	public function get_profil()
	{
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
		return $this->db->get('tb_profil')->row();
	}
  
    public function get_logo()
    {
        $this->db->where('id_akun', $this->session->userdata('id_akun'));
        $q = $this->db->get('tb_profil');
        return $q->logo;
    }

    public function get_akun()
    {
        $this->db->where('id_akun', $this->session->userdata('id_akun'));
        return $this->db->get('tb_akun')->row();
    }

    public function simpan_pwd($pwd)
    {
        $this->db->where('id_akun', $this->session->userdata('id_akun'));
        $this->db->update('tb_akun', $pwd);
    }

    public function getoldpwd()
    {
        $this->db->where('id_akun', $this->session->userdata('id_akun'));
        $q = $this->db->get('tb_akun')->row();
        return $q->password;
    }
    public function get_profil_api($token)
    {
        $this->db->select('p.*,a.username');
        $this->db->from('tb_profil p');
        $this->db->join('tb_akun a','p.id_akun=a.id_akun');
        $this->db->where('a.token',$token);
        return $this->db->get()->row();
    }
}