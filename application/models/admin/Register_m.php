<?php
class Register_m extends MY_Model
{
    public function register($akun, $profil)
    {
        $token = $this->generate_token($akun['username'], $akun['password']);

        $akun['token'] = $token;

        $this->db->insert('tb_akun', $akun);

        $where = "token = '".$akun['token']."'";

        $result = $this->db->get_where('tb_akun', $where);

        $profil['id_akun'] = $result->row()->id_akun;

        $this->db->insert('tb_profil', $profil);
    }

    function generate_token($user, $pass)
    {
        date_default_timezone_set('Asia/Jakarta');
        $time = strftime("%Y%m%d%H%M%S",strtotime(date("Y-m-d H:i:s"))) . substr(microtime(),2,3);

        $hash = md5($user).md5($time).md5($pass);

        return $token = md5($hash);
    }

    function get_token($id)
    {
        $this->db->select('token');
        $this->db->where('id_akun', $id);
        return $this->db->get('tb_akun')->row();
    }
}