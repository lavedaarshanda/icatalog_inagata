<?php 
class Voucher_m extends MY_Model
{
	public function simpan($voucher)
	{
		$voucher = (object)$voucher;

		return $this->db->insert('tb_voucher', $voucher);
	}

	public function update_k($where, $table)
	{

		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function set_data($data, $table)
	{
		$this->db->insert($table, $data);
	}

	public function get_voucher()
	{
		$this->db->where('id > 1');
		$this->db->where('id_akun', $this->session->userdata('id_akun'));
		return $this->db->get('tb_voucher');
	}

	public function get_voucher2($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('tb_voucher');
	}

	public function hapusdata($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tb_voucher');
	}

}
?>