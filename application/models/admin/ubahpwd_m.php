<?php
class Ubahpwd_m extends MY_Model
{
	public function change_pwd($data, $where, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}	

	public function check_old($cond1, $cond2)
	{
		$array = array('password' => $cond1, 'id_akun' => $cond2);

		$this->db->where($array); 

		return $this->db->get('tb_akun')->num_rows();
	}	
}
?>