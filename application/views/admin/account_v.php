<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
  <meta name="viewport" content="width=device-width">

	<title> Lupa Kata Sandi - iCatalog </title>

</head>
<body style="background-color: #f2f2f2">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet">
<br>
<hr>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                          
                    <div class="panel-body">
                        <div class="text-center">
                          <!-- <h3><i class="fa fa-lock fa-4x"></i></h3> -->
                          <img src="<?php echo base_url('assets/img/logo-icatalog.png'); ?>" style="margin-bottom: 20px;margin-top: 10px">
                          <h2 class="text-center">Lupa Kata Sandi?</h2>
                            <div class="panel-body">
                              
                          	<p>Masukkan alamat email untuk me-reset kata sandi anda</p>
                              <form class="form" action="<?php echo site_url('admin/forget/verification'); ?>" method="post">
                                <fieldset>
                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-envelope color-blue"></i></span>
                                      
                                      <input id="emailInput" name="email" placeholder="Alamat Email" class="form-control" type="email" oninvalid="setCustomValidity('Masukkan email dengan benar')" onchange="try{setCustomValidity('')}catch(e){}" required="">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <a href="<?php echo base_url('admin/login'); ?>" class="btn btn-lg btn-default pull-left" style="margin-top: 20px;padding-right: 50px;padding-left: 50px">Login</a><input class="btn btn-lg btn-primary pull-right" value="Reset" type="submit" style="margin-top: 20px;padding-right: 50px;padding-left: 50px">
                                  </div>
                                </fieldset>
                              </form>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>