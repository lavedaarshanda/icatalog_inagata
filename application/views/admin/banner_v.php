<!DOCTYPE html>
<html lang="en" class="">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Banner - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">


    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/jquery-confirm.min.css'); ?>" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/img.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">

<script type="text/javascript">
function conf_del(url) {
        $.confirm({
            title: 'Konfirmasi',
            content: 'Yakin ingin menghapus item ini?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Hapus',
                    btnClass: 'btn-red',
                    action: function() {
                        window.location.href = url;
                    }
                },
                cancel: function () {

                },
            },

        });
    }
</script>
</head>
<body class="sidebar-regular">

<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li class="active">
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse in" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li class="active"><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/voucher'); ?>">
                        <i class="pe-7s-credit"></i>
                        <p>Voucher</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Produk</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
                                <li>
                                    <a href="mailto:contact@inagata.com?subject=Bug iCatalog">
                                        <i class="pe-7s-attention"></i> Laporkan Bug
                                    </a>
                                </li>
                                <li>
                                    <a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
                                        <i class="pe-7s-light"></i> Saran dan Masukan
                                    </a>
                                </li>
                                <li>
                                    <a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
                                        <i class="pe-7s-mail"></i> Kontak Admin
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
                                        <i class="pe-7s-close-circle"></i>
                                        Keluar
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container" style="width: 100%">

                <?php if ($banner_count < 5) { ?>

                    <div class="card col-md-12" style="padding-bottom: 20px">
                        <div id="add" class="">

                            <div class="col-md-6" style="padding-bottom: 20px">
                    <div class="header" style="margin-bottom: 20px">
                        <h4 class="title">Banner Promo atau Promo</h4>
                    </div>
                        <?php if ($banner_count == 0) { ?>
                            <center>Tidak Ada Data.</center>
                        <?php 
                    } else { ?>

                            <p style="margin-left: 15px">Gambar Produk</p>
                            <br>


                            <?php foreach ($banner as $row) { ?>
                            <?php if ($row->gambar == null) { ?>
                                <div>
                                    <figure style="width:auto;margin-left: 10px">
                                        <img src="<?php echo base_url('assets/img/no-image-wide.png'); ?>" class="image-responsive" style="width:100%;" />
                                        <desc style="padding-bottom: 10px;padding-top: 10px;"><center><strong><?php echo $row->nama; ?></strong><br><br><a href="<?php echo base_url('admin/produk/details/' . $row->id_produk); ?>">Ke Produk</a>&nbsp; | &nbsp;<a href="javascript:void(0);" onclick="conf_del('<?php echo base_url('admin/banner/del/' . $row->id); ?>');">Hapus</a></center></desc>
                                    </figure>
                                </div>
                                <br>
                                <br>
                            <?php 
                        } else { ?>
                                <div>
                                    <figure style="width:auto;margin-left: 10px">
                                        <img src="<?php echo base_url('uploads/banner/' . $this->session->userdata('username') . '/' . $row->gambar); ?>" class="image-responsive" style="width:100%;" />
                                        <desc style="padding-bottom: 10px;padding-top: 10px;"><center><strong><?php echo $row->nama; ?></strong><br><br><a href="<?php echo base_url('admin/produk/details/' . $row->id_produk); ?>">Ke Produk</a>&nbsp; | &nbsp;<a href="javascript:void(0);" onclick="conf_del('<?php echo base_url('admin/banner/del/' . $row->id); ?>');">Hapus</a></center></desc>
                                    </figure>
                                </div>
                                <br>
                                <br>
                            <?php 
                        } ?>

                        <?php 
                    }
                } ?>
                </div>

                            <?php echo form_open_multipart('admin/banner/form', array('role' => 'form')); ?>
                            <div class="col-md-6">
                                <div class="header">
                                <h4 class="title">Tambah Banner</h4>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Upload Banner</label>
                                            <input type="file" name="gambar" accept="image/gif, image/jpeg, image/png, image/bmp">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Nama Banner</label>
                                            <input type="text" name="nama" placeholder="Nama Banner" class="form-control"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Link ke produk</label>
                                            <?php
                                            $prod = array(
                                                '' => '- Pilih Produk -',
                                            );

                                            foreach ($produk as $row) {
                                                $prod[$row->id] = $row->nama;
                                            }
                                            $atribut_kategori = 'class="form-control prod"';
                                            echo form_dropdown('id_produk', $prod, '', $atribut_kategori);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <button type="submit" style="margin-left:10px" class="btn btn-fill btn-md btn-success pull-right">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>

                <?php 
            } else { ?>
                    <div class="card col-md-12" style="padding-bottom: 20px">
                        <div id="add" class="">

                            <div class="col-md-6" style="padding-bottom: 20px">
                    <div class="header" style="margin-bottom: 20px">
                        <h4 class="title">Banner Promo atau Promo</h4>
                    </div>
                        <?php if ($banner_count == 0) { ?>
                            <center>Tidak Ada Data.</center>
                        <?php 
                    } else { ?>

                            <p style="margin-left: 15px">Gambar Produk</p>
                            <br>


                            <?php foreach ($banner as $row) { ?>
                            <?php if ($row->gambar == null) { ?>
                                <div>
                                    <figure style="width:auto;margin-left: 10px">
                                        <img src="<?php echo base_url('assets/img/no-image-wide.png'); ?>" class="image-responsive" style="width:100%;" />
                                        <desc style="padding-bottom: 10px;padding-top: 10px;"><center><strong><?php echo $row->nama; ?></strong><br><br><a href="<?php echo base_url('admin/produk/details/' . $row->id_produk); ?>">Ke Produk</a>&nbsp; | &nbsp;<a href="javascript:void(0);" onclick="conf_del('<?php echo base_url('admin/banner/del/' . $row->id); ?>');">Hapus</a></center></desc>
                                    </figure>
                                </div>
                                <br>
                                <br>
                            <?php 
                        } else { ?>
                                <div>
                                    <figure style="width:auto;margin-left: 10px">
                                        <img src="<?php echo base_url('uploads/banner/' . $this->session->userdata('username') . '/' . $row->gambar); ?>" class="image-responsive" style="width:100%;" />
                                        <desc style="padding-bottom: 10px;padding-top: 10px;"><center><strong><?php echo $row->nama; ?></strong><br><br><a href="<?php echo base_url('admin/produk/details/' . $row->id_produk); ?>">Ke Produk</a>&nbsp; | &nbsp;<a href="javascript:void(0);" onclick="conf_del('<?php echo base_url('admin/banner/del/' . $row->id); ?>');">Hapus</a></center></desc>
                                    </figure>
                                </div>
                                <br>
                                <br>
                            <?php 
                        } ?>

                        <?php 
                    }
                } ?>
                </div>

                            <?php echo form_open_multipart('', array('role' => 'form')); ?>
                            <div class="col-md-6">
                                <div class="header">
                                <h4 class="title">Tambah Banner</h4>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Upload Banner</label>
                                            <input type="file" name="gambar" accept="image/gif, image/jpeg, image/png, image/bmp" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Nama Banner</label>
                                            <input type="text" name="nama" placeholder="Nama Banner" class="form-control" disabled></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-left:18px">
                                            <label>Link ke produk</label>
                                            <?php
                                            $prod = array(
                                                '' => '- Pilih Produk -',
                                            );

                                            foreach ($produk as $row) {
                                                $prod[$row->id] = $row->nama;
                                            }
                                            $atribut_kategori = 'class="form-control prod" disabled';
                                            echo form_dropdown('id_produk', $prod, '', $atribut_kategori);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <button type="submit" style="margin-left:10px" class="btn btn-fill btn-md btn-success pull-right" disabled>Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                <?php 
            } ?>

            </div>

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-confirm.min.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>
