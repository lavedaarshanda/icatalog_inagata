<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Detail Produk - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/jquery-confirm.min.css'); ?>" rel="stylesheet" />

</head>
<body class="sidebar-regular">

<script type="text/javascript">

    function conf_url(url) {
        $.confirm({
            title: 'Link',
            content: url,
            type: 'blue',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Kunjungi Link',
                    btnClass: 'btn-blue',
                    action: function() {
                        window.open(url,'_blank');
                        window.open(url);
                    }
                },
                close: function () {

                },
            },

        });
    }

    function conf_del(url) {
        $.confirm({
            title: 'Konfirmasi',
            content: 'Yakin ingin menghapus item ini?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Hapus',
                    btnClass: 'btn-red',
                    action: function() {
                        window.location.href = url;
                    }
                },
                cancel: function () {

                },
            },

        });
    }

</script>

<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li class="active">
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse in" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li class="active"><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/diskon'); ?>">
                        <i class="pe-7s-cash"></i>
                        <p>Diskon</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('admin/produk'); ?>"><i class="fa fa-angle-left"></i> Produk</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bug iCatalog">
																			<i class="pe-7s-attention"></i> Laporkan Bug
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
																			<i class="pe-7s-light"></i> Saran dan Masukan
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
																			<i class="pe-7s-mail"></i> Kontak Admin
																	</a>
															</li>
															<li class="divider"></li>
															<li>
																	<a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
																			<i class="pe-7s-close-circle"></i>
																			Keluar
																	</a>
															</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content buttons-with-margin">
            <div class="container-fluid">


               <div class="row">

                    <div class="col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?php echo $detail->nama; ?></h4>
                                <?php if($detail->kategori == "Uncategorized"){ ?>
                                <p class="category"><span class="label label-default">Belum diberi kategori</span>
                                </p>
                                <?php }else{ ?>
                                <p class="category"><span class="label label-info"><?php echo $detail->kategori; ?></span></p>
                                <?php } ?>
                            </div>
                            <div class="content">

                                <?php if($detail->gambar1 != null){ ?>
                                    <div style="height:auto;position:relative;vertical-align:middle">
                                        <!-- <img src="<?php //echo base_url('uploads/products/'.$detail->gambar1); ?>" class="image-responsive" style="width:100%" /> -->
                                        <?php $i=0;
                                            if($gbr->gambar1 != null) {
                                                $i=$i+1;
                                            }
                                            if($gbr->gambar2 != null) {
                                                $i=$i+1;
                                            }
                                            if($gbr->gambar3 != null) {
                                                $i=$i+1;
                                            }
                                        ?>

                                        <?php if($i > 1){ ?>
                                        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height:auto;width:auto;">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                            </ol>
                                        <?php } ?>

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                            <?php if($gbr->gambar2 != null) { ?>
                                                <div class="item active">
                                                    <img src="<?php echo base_url('uploads/products/'.$this->session->userdata('username').'/'.$detail->gambar1); ?>" alt="gambar1" class="image-responsive" style="width:100%">
                                                </div>

                                                <div class="item">
                                                    <img src="<?php echo base_url('uploads/products/'.$this->session->userdata('username').'/'.$gbr->gambar2); ?>" alt="gambar2" class="image-responsive" style="width:100%">
                                                </div>
                                            <?php }else{ ?>
                                                <div class="">
                                                    <img src="<?php echo base_url('uploads/products/'.$this->session->userdata('username').'/'.$detail->gambar1); ?>" alt="gambar1" class="image-responsive" style="width:100%">
                                                </div>
                                            <?php } ?>

                                            <?php if($gbr->gambar3 != null) { ?>
                                                <div class="item">
                                                    <img src="<?php echo base_url('uploads/products/'.$this->session->userdata('username').'/'.$gbr->gambar3); ?>" alt="gambar2" class="image-responsive" style="width:100%">
                                                </div>
                                            <?php } ?>

                                            <?php if($i > 1) { ?>
                                                <!-- Left and right controls -->
                                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="padding-right: 20px">
                                                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="padding-right: 20px">
                                                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            <?php } ?>
                                        </div>

                                    </div>
                                <?php }else{ ?>
                                    <div style="height:auto;position:relative;vertical-align:middle">
                                        <img src="<?php echo base_url('assets/img/no-image.png'); ?>" class="image-responsive" style="width:100%" />
                                    </div>
                                <?php } ?>

                                <div class="footer">
                                    <div class="legend">
                                        <p><?php echo nl2br($detail->deskripsi); ?></p>

                                    </div>
                                    <hr>
                                    <div class="">
                                    <a href="<?php echo $detail->link; ?>" target="_blank" class="btn btn-fill btn-xs btn-info" style="margin-bottom: 3px"><i class="pe-7s-link"></i> Kunjungi Link</button></a>
                                    &nbsp; <a href="<?php echo base_url('admin/produk/edit/'.$detail->id); ?>" class="btn btn-fill btn-xs btn-warning" style="margin-bottom: 3px"><i class="pe-7s-pen"></i> Ubah</button></a>
                                    &nbsp;<a href="javascript:void(0);" onclick="conf_del('<?php echo base_url('admin/produk/hapus/'.$detail->id); ?>');" class="btn btn-fill btn-xs btn-danger" style="margin-bottom: 3px"><i class="pe-7s-trash"></i> Hapus</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-confirm.min.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    --><!--
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script> -->

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php //echo base_url('assets/light/js/demo.js'); ?>"></script> -->
