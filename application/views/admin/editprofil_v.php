<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Edit Profil - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet">

</head>
<body class="sidebar-regular">
<style type="text/css">
    input.fb
    {
        background-image: url('../assets/img/ic-facebook.png');
        background-position: 12px 10px;
        background-repeat: no-repeat;
    }
    input.yt
    {
        background-image: url('../assets/img/ic-youtube.png');
        background-position: 12px 13px;
        background-repeat: no-repeat;
    }
    input.ig
    {
        background-image: url('../assets/img/ic-instagram.png');
        background-position: 12px 10px;
        background-repeat: no-repeat;
    }
</style>
<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/voucher'); ?>">
                        <i class="pe-7s-credit"></i>
                        <p>Voucher</p>
                    </a>
                </li>

                <li class="active">
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse in" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li class="active"><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="<?php //echo base_url('admin/profil'); ?>"><i class="fa fa-angle-left"></i> Profil </a> -->
                    <a class="navbar-brand" href="#">Edit Profil</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bug iCatalog">
																			<i class="pe-7s-attention"></i> Laporkan Bug
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
																			<i class="pe-7s-light"></i> Saran dan Masukan
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
																			<i class="pe-7s-mail"></i> Kontak Admin
																	</a>
															</li>
															<li class="divider"></li>
															<li>
																	<a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
																			<i class="pe-7s-close-circle"></i>
																			Keluar
																	</a>
															</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content buttons-with-margin">
            <div class="container-fluid">


                    <div class="col-md-8 main-form">
                        <div class="card">
                            <div class="header">

                                <?php if ($this->session->flashdata('message_err')) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;<font size="2px"><?php echo $this->session->flashdata('message_err'); ?></font></p>

                                                </div>
                                            </div>
                                        </div>

                                    <br>
                                <?php 
                            } elseif ($this->session->flashdata('message')) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <p><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;<font size="2px"><?php echo $this->session->flashdata('message'); ?></font></p>

                                                </div>
                                            </div>
                                        </div>

                                    <br>
                                <?php 
                            } ?>

                                <h4 class="title">Ubah profil perusahaan</h4>
                            </div>
                            <div class="content">
                                <?php echo form_open_multipart('admin/editprofil/form', array('role' => 'form')); ?>

                                    <label style="padding-bottom: 5px;padding-top: 10px">TENTANG PERUSAHAAN</label>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Perusahaan</label>
                                                <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama_perusahaan" value="<?php echo $profils->nama_perusahaan; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>No. Telp.</label>
                                                <input type="text" class="form-control" placeholder="Nomor Telepon" name="telp" value="<?php echo $profils->telp; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input type="email" class="form-control" placeholder="Alamat Email" name="email" value="<?php echo $profils->email; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Alamat Perusahaan</label>
                                                <input type="text" class="form-control" placeholder="Alamat Perusahaan" name="alamat" value="<?php echo $profils->alamat; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Deskripsi Perusahaan</label>
                                                <input type="text" class="form-control" placeholder="Deskripsi" name="deskripsi" value="<?php echo $profils->deskripsi; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <label style="padding-bottom: 5px;padding-top: 10px">SOSIAL MEDIA</label>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Instagram</label>
                                                <input type="text" class="form-control ig" placeholder="Link Akun Instagram" name="instagram" value="<?php echo $profils->instagram; ?>"  style="padding-left: 35px">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Facebook</label>
                                                <input type="text" class="form-control fb" placeholder="Link Akun Facebook" name="facebook" value="<?php echo $profils->facebook; ?>"  style="padding-left: 35px">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>YouTube</label>
                                                <input type="text" class="form-control yt" placeholder="Link Akun YouTube" name="youtube" value="<?php echo $profils->youtube; ?>"  style="padding-left: 35px">
                                            </div>
                                        </div>
                                    </div>

                                    <label style="padding-bottom: 5px;padding-top: 10px">VISI & MISI PERUSAHAAN</label>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Visi Perusahaan</label>
                                                <textarea class="form-control span6" rows="5" placeholder="Visi" name="visi" style="resize:none"><?php echo $profils->visi; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Misi Perusahaan</label>
                                                <textarea class="form-control span6" rows="5" placeholder="Misi" name="misi" style="resize:none"><?php echo $profils->misi; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <a href="<?php echo base_url('admin/ubahpwd'); ?>"><u>Ubah Password</u></a>
                                    <button type="submit" class="btn btn-success btn-fill pull-right">Ubah Profil</button>
                                    <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form-right">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Logo Perusahaan</label>
                                                <br>
                                                <?php
                                                $logo = $profils->logo;

                                                if ($logo == null) {
                                                    ?>
                                                        <div style="width:100%;height:auto;position:relative;vertical-align:middle">
                                                            <img id="img" src="<?php echo base_url('assets/img/no-image.png'); ?>" class="image-responsive" height="350px" width="350px" style="border:2px solid black; width: 100%;height:auto;" />
                                                        </div>
                                                <?php

                                            } else {
                                                ?>
                                                        <div style="width:100%;height:auto;position:relative;vertical-align:middle">
                                                            <img id="img" src="<?php echo base_url('uploads/logo/' . $this->session->userdata('username') . '/' . $logo); ?>" class="image-responsive" height="350px" width="350px" style="border:2px solid black; width: 100%;height:auto;" />
                                                        </div>
                                                <?php

                                            }
                                            ?>

                                            </div>

                                                <input type="file" name="logo" id="logo" accept="image/gif, image/jpeg, image/png, image/bmp" style="display: none">

                                                <input type="button" id="file" class="btn btn-fill btn-md btn-success" style="margin-right: 10px" value="Upload Logo" onclick="$('#logo').click();">
                                                <label style="color: #808080;">Size file maksimal 4 mb.</label>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form-right">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Banner Profil</label>
                                                <br>
                                                <?php
                                                $banner = $profils->banner;

                                                if ($banner == null) {
                                                    ?>
                                                        <div style="width:100%;height:auto;position:relative;vertical-align:middle">
                                                            <img id="img_banner" src="<?php echo base_url('assets/img/no-image-wide.png'); ?>" class="image-responsive" height="175px" width="350px" style="border:2px solid black; width: 100%;height:auto;" />
                                                        </div>
                                                <?php

                                            } else {
                                                ?>
                                                        <div style="width:100%;height:auto;position:relative;vertical-align:middle">
                                                            <img id="img_banner" src="<?php echo base_url('uploads/banner_profil/' . $this->session->userdata('username') . '/' . $banner); ?>" class="image-responsive" height="175px" width="350px" style="border:2px solid black; width: 100%;height:auto;" />
                                                        </div>
                                                <?php

                                            }
                                            ?>

                                            </div>

                                                <input type="file" name="banner" id="banner" accept="image/gif, image/jpeg, image/png, image/bmp" style="display: none">

                                                <input type="button" id="file" class="btn btn-fill btn-md btn-success" style="margin-right: 10px" value="Upload Banner" onclick="$('#banner').click();">
                                                <label style="color: #808080;">Rasio ukuran gambar 1:2</label>
                                        </div>

                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

    <script type="text/javascript">
        document.getElementById("logo").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("img").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
            // alert('This file size is: ' + (this.files[0].size).toFixed() + " Byte");
        };

        document.getElementById("banner").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("img_banner").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

    </script>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <!-- <script async="" src="<?php //echo base_url('assets/light/js/analytics.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php //echo base_url('assets/light/js/demo.js'); ?>"></script> -->
