<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
  <meta name="viewport" content="width=device-width">

	<title> Lupa Kata Sandi - iCatalog </title>

</head>
<body style="background-color: #f2f2f2">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet">
<br>
<hr>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                          
                    <div class="panel-body">
                        <div class="text-center">
                          <!-- <h3><i class="fa fa-lock fa-4x"></i></h3> -->
                          <img src="<?php echo base_url('assets/img/logo-icatalog.png'); ?>" style="margin-bottom: 20px;margin-top: 10px">
                          <h2 class="text-center"><?php echo $this->session->flashdata('ttlfrg'); ?></h2>
                            <div class="panel-body">
                              
                          	<p><?php echo $this->session->flashdata('msgfrg'); ?></p>
                            
                            <a href="<?php echo base_url('admin/login'); ?>" class="btn btn-lg btn-primary btn-block" style="margin-top: 20px;padding-right: 50px;padding-left: 50px"><i class="fa fa-angle-left"></i> &nbsp; Kembali ke halaman Login</a>

                              
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>