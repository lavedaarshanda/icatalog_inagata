<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Kategori - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/jquery-confirm.min.css'); ?>" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">

</head>
<body class="sidebar-regular">

<script type="text/javascript">
    function editkateg(id, kateg){
        $.confirm({
            title: 'Ubah Kategori',
            content: '' +
            '<form action="" method="post" class="formName">' +
            '<div class="form-group">' +
            '<label>Nama Kategori</label>' +
            '<input name="kategori" id="kategori" type="text" placeholder="Nama Kategori" class="kategori form-control" value="' + kateg + '" required autofocus />' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Save',
                    btnClass: 'btn-blue',
                    action: function () {
                        var k = this.$content.find('.kategori').val();
                        if(!k){
                            $.alert('Masukkan nama kategori yang benar');
                                return false;
                        }
                        window.location.href = "kategori/update/" + id + "/" + k;
                        $.dialog({
                            title: 'Sukses',
                            content: 'Item berhasil dirubah',
                            type: 'green',
                            typeAnimated: true,
                        });
                        //window.location.href = "kategori";
                    }
                },
                cancel: function () {
                    //close
                },
            },

            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

    function addkateg(){
        $.confirm({
            title: 'Tambah Kategori',
            content: '' +
            '<form action="" method="post" class="formName">' +
            '<div class="form-group">' +
            '<label>Nama Kategori</label>' +
            '<input name="kategori" id="kategori" type="text" placeholder="Nama Kategori" class="kategori form-control" required autofocus />' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Save',
                    btnClass: 'btn-blue',
                    action: function () {
                        var k = this.$content.find('.kategori').val();
                        if(!k){
                            $.alert('Masukkan nama kategori yang benar');
                                return false;
                        }
                        window.location.href = "kategori/add/" + k;
                        $.dialog({
                            title: 'Sukses',
                            content: 'Item berhasil ditambahkan',
                            type: 'green',
                            typeAnimated: true,
                        });
                        //window.location.href = "kategori";
                    }
                },
                cancel: function () {
                    //close
                },
            },

            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

    function conf_del(url) {
        $.confirm({
            title: 'Konfirmasi',
            content: 'Yakin ingin menghapus item ini?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Hapus',
                    btnClass: 'btn-red',
                    action: function() {
                        window.location.href = url;
                    }
                },
                cancel: function () {

                },
            },

        });
    }
</script>

<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li class="active">
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/voucher'); ?>">
                        <i class="pe-7s-credit"></i>
                        <p>Voucher</p>
                    </a>
                </li>
                
                <li>
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Kategori</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bug iCatalog">
																			<i class="pe-7s-attention"></i> Laporkan Bug
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
																			<i class="pe-7s-light"></i> Saran dan Masukan
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
																			<i class="pe-7s-mail"></i> Kontak Admin
																	</a>
															</li>
															<li class="divider"></li>
															<li>
																	<a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
																			<i class="pe-7s-close-circle"></i>
																			Keluar
																	</a>
															</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content buttons-with-margin">
            <div class="container-fluid">


               <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Daftar Kategori</h4>
                                <p class="category">Tambah, Ubah, Hapus Kategori Disini.</p>
                            </div>
                            <hr>
                            <button type="button" class="btn btn-fill btn-xs btn-success" onclick="addkateg();" style="margin-bottom:5px;margin-left:15px;"><i class="fa fa-plus"></i> Tambah Kategori Baru</button>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">

                                <?php if ($kategori != null) { ?>
                                    <thead>
                                        <th>No.</th>
                                        <th>Kategori</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;
                                    foreach ($kategori as $row) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $row->kategori; ?></td>
                                            <td><a href="javascript:void(0);" onclick="editkateg('<?php echo $row->id; ?>', '<?php echo $row->kategori; ?>');"><button type="button" class="btn btn-xs btn-warning btn-fill"><i class="pe-7s-pen"></i> Ubah</button></a> &nbsp; <a  href="javascript:void(0);" onclick="conf_del('<?php echo "kategori/hapus/" . $row->id; ?>');" class="btn btn-xs btn-danger btn-fill"> <i class="pe-7s-trash"></i> Hapus </a>
                                            </td>
                                        </tr>
                                    <?php 
                                } ?>
                                    </tbody>
                                <?php 
                            } else { ?>
                                    <tbody>
                                        <th><center>Tidak ada data.</center></th>
                                    </tbody>
                                <?php 
                            } ?>

                                </table>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <!-- <script async="" src="<?php //echo base_url('assets/light/js/analytics.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-confirm.min.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php //echo base_url('assets/light/js/demo.js'); ?>"></script> -->
