<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Profil - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">

</head>
<body class="sidebar-regular">

<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/voucher'); ?>">
                        <i class="pe-7s-credit"></i>
                        <p>Voucher</p>
                    </a>
                </li>
                
                <li class="active">
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse in" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li class="active"><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Profil</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bug iCatalog">
																			<i class="pe-7s-attention"></i> Laporkan Bug
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
																			<i class="pe-7s-light"></i> Saran dan Masukan
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
																			<i class="pe-7s-mail"></i> Kontak Admin
																	</a>
															</li>
															<li class="divider"></li>
															<li>
																	<a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
																			<i class="pe-7s-close-circle"></i>
																			Keluar
																	</a>
															</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content buttons-with-margin">
            <div class="container-fluid">


               <div class="col-md-12">
                        <div class="card card-user">
                            <div class="image" style="height:200px">
                                <?php if ($profil->banner == null) { ?>

                                    <img src="<?php echo base_url('assets/img/no-image-wide.png'); ?>" alt="..." style="object-position: center" />

                                <?php 
                            } else { ?>

                                    <img src="<?php echo base_url('uploads/banner_profil/' . $this->session->userdata('username') . '/' . $profil->banner); ?>" alt="..." style="object-position: center" />

                                <?php 
                            } ?>
                            </div>
                            <div class="content">
                                <div class="author">

                                     <?php if ($profil->logo != null) { ?>
                                        <img class="avatar border-gray" src="<?php echo base_url('uploads/logo/' . $this->session->userdata('username') . '/' . $profil->logo); ?>" alt="..."/>
                                    <?php 
                                } else { ?>
                                        <img class="avatar border-gray" src="<?php echo base_url('assets/img/no-image.png'); ?>" alt="..."/>
                                    <?php 
                                } ?>
                                      <h4 class="title"><?php echo $profil->nama_perusahaan; ?><br />
                                         <small><?php echo $this->session->userdata('username'); ?></small>
                                      </h4>

                                </div>
                                <?php if ($profil->deskripsi != null) { ?>
                                    <br>
                                    <p class="description text-center"> <?php echo "&quot; " . $profil->deskripsi . " &quot;"; ?> </p>
                                <?php 
                            } else { ?>
                                    <br>
                                    <p class="description text-center"> <?php echo '&quot; <font color="#808080"><i>Deskripsi Kosong</i></font> &quot;'; ?> </p>
                                <?php 
                            } ?>
                                <br>
                                <center>

                                <?php if ($profil->visi != null || $profil->misi != null) { ?>
                                    <table border="1px">
                                    <tr>
                                    <td><center><strong>Visi:</strong></center></td>
                                    <td><center><strong>Misi:</strong></center></td>
                                    </tr>
                                    <tr>
                                    <td style="width: 200px;padding-right: 10px;padding-left: 10px;"><p> <?php echo nl2br($profil->visi); ?> </p>
                                    </td>
                                    <td style="width: 200px;padding-right: 10px;padding-left: 10px;"><p> <?php echo nl2br($profil->misi); ?> </p>
                                    </td>
                                    </tr>
                                    </table>
                                <?php 
                            } ?>

                                </center>
                                <br>
                                <center><a href="editprofil" class="btn btn-fill btn-warning"><i class="fa fa-wrench"></i> &nbsp;Ubah Profil</a></center>
                            </div>
                            <hr>
                            <div class="text-center">
                                <?php if ($profil->facebook) { ?>
                                    <a href="http://www.facebook.com/search/?query=<?php echo $profil->facebook; ?>" target="_blank" class="btn btn-simple"><i class="fa fa-facebook-square"></i> <?php echo $profil->facebook; ?></a>
                                <?php 
                            } ?>

                                <?php if ($profil->instagram) { ?>
                                    <a href="http://www.instagram.com/<?php echo $profil->instagram; ?>" target="_blank" class="btn btn-simple"><i class="fa fa-instagram"></i> <?php echo $profil->instagram; ?></a>
                                <?php 
                            } ?>

                                <?php if ($profil->youtube) { ?>
                                    <a href="http://www.youtube.com/<?php echo $profil->youtube; ?>" target="_blank" class="btn btn-simple"><i class="fa fa-youtube-play"></i> <?php echo $profil->youtube; ?></a>
                                <?php 
                            } ?>

                            </div>
                        </div>
                    </div>


            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <!-- <script async="" src="<?php //echo base_url('assets/light/js/analytics.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php //echo base_url('assets/light/js/demo.js'); ?>"></script> -->
