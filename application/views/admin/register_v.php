<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('asset/img/favicon.ico')?>">

    <title>iCatalog Admin Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/signin.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php //echo base_url('asset/html5shiv/html5shiv.min.js'); ?>"></script>
    <script src="<?php //echo base_url('asset/respond/respond.min.js'); ?>"></script>
    <![endif]-->
</head>
<body>
<style>
   html,body
    {
        height: 100%;
        margin: 0;
        padding: 0;
        background: #fff;
    }
    .login
    { 
        width: 102px; 
        height: 38px; 
        border-radius: 4px; 
        background-color: #3d78d8; 
    }
    .register 
    {
        width: 160px;
        height: 38px;
        border-radius: 4px;
        background-color: #0a224c;
    }
     input.email
    {
        background-image: url('../../../assets/img/icon-email.png');
        background-position: 14px 7px;
        background-repeat: no-repeat;
    }
    input.telp
    {
        background-image: url('../../../assets/img/icon-handphone.png');
        background-position: 14px 7px;
        background-repeat: no-repeat;
    }
     input.name
    {
        background-image: url('../../../assets/img/icon-name.png');
        background-position: 14px 7px;
        background-repeat: no-repeat;
    }
     input.uname
    {
        background-image: url('../../../assets/img/icon-username.png');
        background-position: 14px 7px;
        background-repeat: no-repeat;
    }
    input.pwd
    {
        background-image: url('../../../assets/img/icon-katasandi.png');
        background-position: 16px 7px;
        background-repeat: no-repeat;
    }
</style>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <article id="slider">
                    <input checked type='radio' name='slider' id='slide1'/>
                    <input type='radio' name='slider' id='slide2'/>
                    <input type='radio' name='slider' id='slide3'/>
                    <div id="slides" style="margin: -20px;">
                        <div id="container">
                            <div class="inner">
                                <article>
                                    <img src=" <?php echo base_url('assets/img/img-login.png'); ?>"/>
                                </article>
                                <article>
                                    <img src=" <?php echo base_url('assets/img/img-login.png'); ?>"/>
                                </article>
                                <article>
                                    <img src=" <?php echo base_url('assets/img/img-login.png'); ?>"/>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div id="commands">
                        <label for='slide1'></label>
                        <label for='slide2'></label>
                        <label for='slide3'></label>
                    </div>
                    <div id="active">
                        <label for='slide1'></label>
                        <label for='slide2'></label>
                        <label for='slide3'></label>
                    </div>
                </article>

            </div>
        </div>

        <div class="col-md-4">

            <?php if ($this->session->flashdata('regmsg')){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <span class="glyphicon glyphicon-exclamation-triangle" aria-hidden="true"></span>
                                <?php echo $this->session->flashdata('regmsg'); ?>                                
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="row" style="padding-top:3%;margin: 30px;" >
                <form role="form" action="<?php echo site_url('admin/register/reg')?>" method="post">
                    <h2 class="form-signin-heading" style="padding-bottom: 10px"><img src="<?php echo base_url('assets/img/logo-icatalog.png'); ?>"></h2>
                    <label style="padding-top:10px"><font color="#8f8f88">USERNAME</font></label>
                    <input type="text" name="username" class="uname form-control" style="padding-left: 35px" autofocus="autofocus" required>
                    <label style="padding-top:10px"><font color="#8f8f88">NAMA PERUSAHAAN</font></label>
                    <input type="text" name="nama" class="name form-control" style="padding-left: 35px" required>
                    <label style="padding-top:10px"><font color="#8f8f88">NO HANDPHONE/TELEPHONE</font></label>
                    <input type="text" name="no_telp" class="telp form-control" style="padding-left: 35px" required>
                    <label style="padding-top:10px"><font color="#8f8f88">EMAIL</font></label>
                    <input type="text" name="email" class="email form-control" style="padding-left: 35px" required>
                    <label style="padding-top:10px"><font color="#8f8f88">KATA SANDI</font></label>
                    <input type="password" name="password" class="pwd form-control" style="padding-left: 35px" required>
                    <label class="pull-left" style="margin-top: 20px;color: #8f8f88;">Sudah punya akun? <a href="<?php echo base_url('admin/login'); ?>">Masuk</a></label><button class="btn register pull-right" type="submit" style="margin-top: 10px; color: #fff;">DAFTAR SEKARANG</button>
                </form>
            </div>

            <div class="row" style="padding-top: 50px">
                <div class="footer">
                    <h6><center>&copy; 2017 <a href="http://www.inagata.com" target="_blank">INAGATA TECHNOSMITH</a>. ALL RIGHTS RESERVED.</center></h6>
                </div>
            </div>
        </div>
    </div>

</div> <!-- /container -->

<script src="<?php echo base_url('asset/jquery_1_11_1/jquery-1.11.1.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/bootstrap_3_2_0/js/bootstrap.min.js'); ?>"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url('assets/js/ie10-viewport-bug-workaround.js');?>"></script>
</body>
</html>