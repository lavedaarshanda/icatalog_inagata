<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
  <meta name="viewport" content="width=device-width">

	<title> Lupa Kata Sandi - iCatalog </title>

</head>
<body style="background-color: #f2f2f2">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet">
<br>
<hr>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                          
                    <?php if($this->session->flashdata('msgrst')){ ?>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                              <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;<font size="2px"><?php echo $this->session->flashdata('msgrst'); ?></font></p>
                            
                          </div>
                        </div>
                      </div>

                      <br>
                    <?php } ?>

                    <div class="panel-body">
                        <div class="text-center">
                          <!-- <h3><i class="fa fa-lock fa-4x"></i></h3> -->
                          <img src="<?php echo base_url('assets/img/logo-icatalog.png'); ?>" style="margin-bottom: 20px;margin-top: 10px">
                          <h3 class="text-center">Reset Kata Sandi</h3>
                            <div class="panel-body">
                            
                            <p>Silahkan reset kata sandi anda.</p>
                          	<form class="form" method="post" action="<?php echo base_url('admin/forget/resetpwd/'.$id_akun_reset); ?>" style="margin-top:20px;">

                              <fieldset>
                                <div class="form-group">
                                  <label>Masukkan kata sandi baru</label>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock color-blue"></i></span>
                                      
                                    <input id="password" name="password" placeholder="kata sandi baru" class="form-control" type="password" required="">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label>Konfirmasi kata sandi baru</label>
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock color-blue"></i></span>
                                      
                                    <input id="confirmpassword" name="confirmpassword" placeholder="Konfirmasi kata sandi baru" class="form-control" type="password" required="">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <a href="<?php echo base_url('admin/login'); ?>" class="btn btn-lg btn-default pull-left" style="margin-top: 20px;padding-right: 50px;padding-left: 50px">Batal</a><input class="btn btn-lg btn-primary pull-right" value="Reset" type="submit" style="margin-top: 20px;padding-right: 50px;padding-left: 50px">
                                </div>
                              </fieldset>

                            </form>
                              
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  var password = document.getElementById("password"), confirmpassword = document.getElementById("confirmpassword");

    function validatePassword(){
      if(password.value != confirmpassword.value) {
        confirmpassword.setCustomValidity("Kata sandi tidak sama");
      } else {
        confirmpassword.setCustomValidity('');
      }
    }

  password.onchange = validatePassword;
  confirmpassword.onkeyup = validatePassword;
</script>
</body>
</html>