<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Edit Profil - iCatalog</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/light/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet">

</head>
<body class="sidebar-regular">
<style type="text/css">
    input.pwd
    {
        background-image: url('../assets/img/icon-katasandi.png');
        background-position: 16px 7px;
        background-repeat: no-repeat;
    }
</style>
<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
		<!-- <div class="logo logo-mini">
			<center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
		</div> -->

    	<div class="sidebar-wrapper">

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('admin/home'); ?>">
                        <i class="pe-7s-home"></i>
                        <p>Beranda</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#produk" class="collapsed" aria-expanded="false">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Produk
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="produk" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                            <li><a href="<?php echo base_url('admin/tambahproduk'); ?>">Tambah Produk</a></li>
                            <li><a href="<?php echo base_url('admin/banner'); ?>">Banner</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/kategori'); ?>">
                        <i class="pe-7s-ticket"></i>
                        <p>Kategori</p>
                    </a>
                </li>

                <li class="active">
                    <a data-toggle="collapse" href="#profil" aria-expanded="false" class="collapsed">
                        <i class="pe-7s-user"></i>
                        <p>Profil
                           <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse in" id="profil" aria-expanded="false" style="height: auto;">
                        <ul class="nav">
                            <li><a href="<?php echo base_url('admin/profil'); ?>">Lihat Profil</a></li>
                            <li class="active"><a href="<?php echo base_url('admin/editprofil'); ?>">Edit Profil</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
    	</div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>

    <div class="main-panel" >
        <nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
				</div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="<?php //echo base_url('admin/profil'); ?>"><i class="fa fa-angle-left"></i> Profil </a> -->
                    <a class="navbar-brand" href="<?php echo base_url('admin/editprofil'); ?>"><i class="fa fa-angle-left"></i> Edit Profil</a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
    								<b class="caret"></b>
    							</p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bug iCatalog">
																			<i class="pe-7s-attention"></i> Laporkan Bug
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Saran dan Masukan iCatalog">
																			<i class="pe-7s-light"></i> Saran dan Masukan
																	</a>
															</li>
															<li>
																	<a href="mailto:contact@inagata.com?subject=Bantuan iCatalog">
																			<i class="pe-7s-mail"></i> Kontak Admin
																	</a>
															</li>
															<li class="divider"></li>
															<li>
																	<a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
																			<i class="pe-7s-close-circle"></i>
																			Keluar
																	</a>
															</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content buttons-with-margin">
            <div class="container-fluid">


                    <div class="col-md-6 col-md-offset-3 main-form">
                        <div class="card">
                            <div class="header">

                                <?php if($this->session->flashdata('error') == 100){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>

                                                Kata sandi lama salah. Silahkan coba lagi.

                                                </div>
                                            </div>
                                        </div>

                                    <br>

                                <?php } ?>

                                <?php if($this->session->flashdata('error') == 200){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>

                                                Kata sandi baru tidak sama. Silahkan konfirmasi kembali.

                                                </div>
                                            </div>
                                        </div>

                                    <br>

                                <?php } ?>

                                <?php if($this->session->flashdata('success') == 120){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>

                                                Kata sandi berhasil dirubah.

                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                <?php } ?>

                                <h4 class="title">Ubah informasi akun</h4>
                            </div>
                            <div class="content">
                                <?php echo form_open_multipart('admin/ubahpwd/form', array('role'=>'form')); ?>

                                    <label style="padding-bottom: 5px;padding-top: 10px">KATA SANDI</label>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kata sandi lama</label>
                                                <input type="password" class="form-control pwd" placeholder="" name="old_pwd" value="" style="padding-left: 35px" autofocus="autofocus" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kata sandi baru</label>
                                                <input type="password" class="form-control pwd" placeholder="" name="new_pwd" value="" style="padding-left: 35px" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Masukkan kembali kata sandi baru</label>
                                                <input type="password" class="form-control pwd" placeholder="" name="con_pwd" value="" style="padding-left: 35px" required>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <button type="submit" class="btn btn-success btn-fill pull-right">Ubah Kata Sandi</button>
                                    <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>

            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.
                </p>
                </center>
            </div>
        </footer>

</div>

    <script type="text/javascript">
        document.getElementById("logo").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("img").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

        document.getElementById("banner").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("img_banner").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

    </script>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <!-- <script async="" src="<?php //echo base_url('assets/light/js/analytics.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
	<script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

	<!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<!-- <script src="<?php //echo base_url('assets/light/js/demo.js'); ?>"></script> -->
