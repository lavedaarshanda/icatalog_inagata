<!DOCTYPE html>

<html lang="en" class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Dashboard - SuperAdmin@iCatalog</title>

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/light/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet">

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/jquery-confirm.min.css'); ?>" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/light/css/css.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet">

</head>
<body class="sidebar-regular">

<div class="wrapper">
    <div class="sidebar" data-color="red" data-image="<?php echo base_url('assets/img/sidebar-4.jpg'); ?>">
        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="logo">
            <center><img src="<?php echo base_url('assets/img/ic_logo.png'); ?>" class="logo-text" style="width:100px;"></center>
        </div>
        <!-- <div class="logo logo-mini">
            <center><img src="<?php //echo base_url('assets/img/ic_logo_mini.png'); ?>" class="logo-text" style="width:1px;"></center>
        </div> -->

        <div class="sidebar-wrapper">

            <ul class="nav">
                <li class="active">
                    <a href="<?php echo base_url('superadmin/shome'); ?>">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo base_url('superadmin/suser'); ?>">
                        <i class="pe-7s-users"></i>
                        <p>Daftar Pengguna</p>
                    </a>
                </li>

            </ul>
        </div>
    <div class="sidebar-background" style="background-image: url(./assets/img/sidebar-4.jpg) "></div></div>
    <div class="main-panel" >
        <nav class="navbar navbar-default" style="background-color: #424242">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-danger btn-fill btn-round btn-icon">
                        <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                        <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><font color="white">Dashboard</font></a>
                </div>
                <div class="collapse navbar-collapse">

                    <!-- <form class="navbar-form navbar-left navbar-search-form" role="search">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" value="" class="form-control" placeholder="Search...">
                        </div>
                    </form> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Selengkapnya
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
                                <li>
                                    <a href="#">
                                        <i class="pe-7s-tools"></i> Pengaturan
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo base_url('admin/logout'); ?>" class="text-danger">
                                        <i class="pe-7s-close-circle"></i>
                                        Keluar
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content" style="width: 100%">
            <div class="container-fluid">

              <div class="row">
                  <div class="col-md-6">
                      <div class="card">
                          <div class="header" style="background-color: #FF5733">
                              <div class="row">
                                  <div class="col-md-3">
                                      <font color="#ffffff"><i class="fa fa-users fa-5x"></i></font>
                                  </div>
                                  <div class="col-md-9 text-right">
                                      <font color="#ffffff"><div class="huge"><h1><?php echo $users_count; ?></h1></div>
                                      <div>Pengguna Terdaftar</div></font>
                                  </div>
                              </div>
                          </div>
                          <a href="<?php echo base_url('superadmin/suser'); ?>">
                              <div class="content">
                                  <span class="pull-left">Lihat Detail</span>
                                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                  <div class="clearfix"></div>
                              </div>
                          </a>
                      </div>
                  </div>


                    <div class="col-md-6">
                        <div class="card">
                            <div class="header" style="background-color: #0BE1DE">
                                <div class="row">
                                    <div class="col-md-3">
                                        <font color="#ffffff"><i class="fa fa-cloud fa-5x"></i></font>
                                    </div>
                                    <div class="col-md-9 text-right">
                                        <font color="#ffffff"><div class="huge"><h1><?php echo $users_count; ?>MB</h1></div>
                                        <div>Total Penyimpanan Terpakai</div></font>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('superadmin/suser'); ?>">
                                <div class="content">
                                    <span class="pull-left">Lihat Detail</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                  </div>

            </div>
        </div>


        <footer class="footer" style="background-color: #424242">
            <div class="container-fluid">
                <center>
                <p class="copyright">
                    <font color="white">Created by <a href="http://www.inagata.com/" target="_blank">Inagata Technosmith</a>, 2017.</font>
                </p>
                </center>
            </div>
        </footer>
</div>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="<?php echo base_url('assets/light/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/light/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


    <!--  Forms Validations Plugin -->
    <script src="<?php echo base_url('assets/light/js/jquery.validate.min.js'); ?>"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="<?php echo base_url('assets/light/js/moment.min.js'); ?>"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-datetimepicker.js'); ?>"></script>

    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-selectpicker.js'); ?>"></script>

    <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-checkbox-radio-switch-tags.js'); ?>"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url('assets/light/js/chartist.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-notify.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-confirm.min.js'); ?>"></script>

    <!-- Sweet Alert 2 plugin -->
    <script src="<?php echo base_url('assets/light/js/sweetalert2.js'); ?>"></script>

    <!-- Vector Map plugin -->
    <script src="<?php echo base_url('assets/light/js/jquery-jvectormap.js'); ?>"></script>

    <!--  Google Maps Plugin    -->
    <script src="<?php echo base_url('assets/light/js/js.js'); ?>"></script>

    <!-- Wizard Plugin    -->
    <script src="<?php echo base_url('assets/light/js/jquery.bootstrap.wizard.min.js'); ?>"></script>

    <!--  Bootstrap Table Plugin    -->
    <script src="<?php echo base_url('assets/light/js/bootstrap-table.js'); ?>"></script>

    <!--  Plugin for DataTables.net  -->
    <script src="<?php echo base_url('assets/light/js/jquery.datatables.js'); ?>"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url('assets/light/js/fullcalendar.min.js'); ?>"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
    <script src="<?php echo base_url('assets/light/js/light-bootstrap-dashboard.js'); ?>"></script>

    <!--   Sharrre Library    -->
    <script src="<?php echo base_url('assets/light/js/jquery.sharrre.js'); ?>"></script>
