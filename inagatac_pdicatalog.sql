-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2020 at 07:30 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inagatac_pdicatalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun`
--

CREATE TABLE `tb_akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akun`
--

INSERT INTO `tb_akun` (`id_akun`, `username`, `password`, `token`) VALUES
(1, 'inagata', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2810045c4c77da80aec03dd6a40088e1'),
(2, 'asroriasrori', '202cb962ac59075b964b07152d234b70', 'fd32e01611ebd47d750b6736d483ac5c'),
(3, 'demos', '9e8e2db3bc5ed9dbf33f7bcd0ce401a7', '02be97f2990c10b9a73b7a60eb6622e9'),
(4, 'tester', 'f5d1278e8109edd94e1e4197e04873b9', '9788763246f1ff52144e166427533ac2'),
(5, 'ninno', 'e10adc3949ba59abbe56e057f20f883e', '38e0da3e56fc3807f7836cde4f8ea783'),
(6, 'febri', '4689c75fd0935ff5818d62fd2083ed98', '5b3b3032790cca858f409831eafcce87'),
(9, 'lavedaarshanda', '50a18e391a4137b2aefb5afe93a498ae', '08a69f748908b575d5991cc67fafe77a');

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE `tb_banner` (
  `id` varchar(100) NOT NULL,
  `id_akun` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_banner`
--

INSERT INTO `tb_banner` (`id`, `id_akun`, `nama`, `gambar`, `id_produk`) VALUES
('20190221102326144', 1, 'tes 2', 'file_20170322100218333.jpg', 2),
('20190226132419605', 1, 'tes 2', 'bannerpd_20190226132419605.jpeg', 19),
('20190313105456705', 1, 'Star', 'bannerpd_20190313105456705.jpg', 25),
('20190314154441014', 1, 'ui layer', 'bannerpd_20190314154441014.png', 10),
('20190423160119746', 3, 'Ayam Geprek', 'bannerpd_20190423160119746.jpg', 53),
('20190423160210031', 3, 'Martabak', 'bannerpd_20190423160210031.jpg', 68),
('20190423160239599', 3, 'Jamu', 'bannerpd_20190423160239599.jpg', 70),
('20190423160314704', 3, 'Bandeng Presto', 'bannerpd_20190423160314704.jpg', 54);

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_keranjang`
--

CREATE TABLE `tb_detail_keranjang` (
  `id_detail_keranjang` int(11) NOT NULL,
  `id_varian` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `id_keranjang` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `id_akun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `kategori`, `id_akun`) VALUES
(1, 'uncategorized', 1),
(2, 'Website', 1),
(3, 'Android', 1),
(5, 'Game', 1),
(11, 'Mobile', 4),
(12, 'Website', 4),
(13, 'Lalapan', 3),
(14, 'Minuman', 3),
(15, 'Cemilan', 3),
(17, 'Sayur dan Buah', 8),
(18, 'Biji-bijian', 8),
(19, 'Fashion', 9),
(21, 'Fotografi', 9),
(22, 'Food', 9),
(23, 'Make Up', 9),
(24, 'Skincare', 9);

-- --------------------------------------------------------

--
-- Table structure for table `tb_keranjang`
--

CREATE TABLE `tb_keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_kate` int(11) NOT NULL,
  `link` varchar(100) NOT NULL,
  `gambar1` varchar(100) NOT NULL,
  `gambar2` varchar(100) NOT NULL,
  `gambar3` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `id_akun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id`, `nama`, `deskripsi`, `id_kate`, `link`, `gambar1`, `gambar2`, `gambar3`, `stok`, `harga_pokok`, `harga_jual`, `id_akun`) VALUES
(1, 'Go Futsal', 'Menyediakan tempat futsal secara online untuk booking lebih baik lagi\n', 2, 'http://facebook.com', 'file_Go_Futsal_1.jpg', '', '', 0, 0, 0, 1),
(3, 'Agung Trans Web', '', 3, 'http://', 'file_Agung_Trans_Web_1.jpg', '', '', 0, 0, 0, 1),
(4, 'BevlourWeb', '', 3, 'http://', 'file_Bevlour_Web_1.jpg', '', '', 0, 0, 0, 1),
(5, 'Butik', '', 3, 'http://', 'file_Butik_1.jpg', '', '', 0, 0, 0, 1),
(6, 'Depo Loundry Web', '', 3, 'http://', 'file_Depo_Loundry_Web_1.jpg', '', '', 0, 0, 0, 1),
(7, 'Easy Teacher', '', 3, 'http://', 'file_Easy_Teacher_1.jpg', '', '', 0, 0, 0, 1),
(8, 'Edupongo', '', 3, 'http://', 'file_Edupongo_1.jpg', '', '', 0, 0, 0, 1),
(10, 'Inagiude', '', 3, 'http://', 'file_Inagiude_1.jpg', '', '', 0, 0, 0, 1),
(11, 'iOS Downloader', '', 3, 'http://', 'file_iOS_Downloader_1.jpg', '', '', 0, 0, 0, 1),
(12, 'Kebun Bibit App', '', 3, 'http://', 'file_Kebun_Bibit_App_1.jpg', '', '', 0, 0, 0, 1),
(13, 'Nobook', '', 3, 'http://', 'file_Nobook_1.jpg', '', '', 0, 0, 0, 1),
(14, 'Omah Nyewo', '', 3, 'http://', 'file_Omah_Nyewo_1.jpg', '', '', 0, 0, 0, 1),
(15, 'Pajang Barang Network', '', 3, 'http://', 'file_Pajang_Barang_Network_1.jpg', '', '', 0, 0, 0, 1),
(16, 'Paradise Group Web', '', 3, 'http://', 'file_Paradise_Group_Web_1.jpg', '', '', 0, 0, 0, 1),
(17, 'Parenting Control', '', 3, 'http://', 'file_Parenting_Control_1.jpg', '', '', 0, 0, 0, 1),
(18, 'PSB SMK Telkom Malang Web', '', 3, 'http://', 'file_PSB_SMK_Telkom_Malang_Web_1.jpg', '', '', 0, 0, 0, 1),
(19, 'Si Monev', '', 3, 'http://', 'file_Si_Monev_1.jpg', '', '', 0, 0, 0, 1),
(20, 'Taqorrubat', '', 3, 'http://', 'file_Taqorrubat_1.jpg', '', '', 0, 0, 0, 1),
(21, 'True Face', '', 3, 'http://', 'file_True_Face_1.jpg', '', '', 0, 0, 0, 1),
(22, 'Wiratama Web', '', 3, 'http://', 'file_Wiratama_Web_1.jpg', '', '', 0, 0, 0, 1),
(38, 'QA', '', 1, 'http://', 'product_tester_QA_1.PNG', '', '', 0, 0, 0, 4),
(39, 'Bug', 'mengenai error ', 11, 'http://', 'product_tester_Bug_1.PNG', '', '', 0, 0, 0, 4),
(40, 'Art', 'Art', 11, 'http://', 'product_tester_Art_1.jpg', 'product_tester_Art_2.png', 'product_tester_Art_3.jpg', 0, 0, 0, 4),
(41, 'Sekolahku', '', 12, 'http://', '', '', '', 0, 0, 0, 4),
(50, 'Ayam Bakar', '', 13, 'http://', 'product_demos_Ayam_Bakar_1.jpg', '', '', 0, 0, 0, 3),
(51, 'Ayam Goreng', '', 13, 'http://', 'product_demos_Ayam_Goreng_1.jpg', '', '', 0, 0, 0, 3),
(52, 'Ayam Krispi', '', 13, 'http://', 'product_demos_Ayam_Krispi_1.jpg', '', '', 0, 0, 0, 3),
(53, 'Ayam Geprek', 'Ayam Geprek Mbok enak lezat dengan 3 pilihan sambel (hijau, merah, dan tomat). Rasa nyuss bikin ketagihan, 1 porsi bisa kamu dapatkan hanya dengan seharga Rp6000,- (tanpa nasi). Yuk nikmati bareng sambil ngobrol asyik dengan temen kamu. ', 13, 'http://', 'file_Ayam_Geprek_1.jpg', 'product_demos_Ayam_Geprek_2.jpg', 'product_demos_Ayam_Geprek_3.jpg', 0, 0, 0, 3),
(54, 'Bandeng Presto', '', 13, 'http://', 'product_demos_Bandeng_Presto_1.jpg', '', '', 0, 0, 0, 3),
(55, 'Ikan Lele', '', 13, 'http://', 'product_demos_Ikan_Lele_1.jpg', '', '', 0, 0, 0, 3),
(56, 'Ikan Mujaer', '', 13, 'http://', 'product_demos_Ikan_Mujaer_1.jpg', '', '', 0, 0, 0, 3),
(57, 'Jamur Krispi', '', 13, 'http://', 'product_demos_Jamur_Krispi_1.jpg', '', '', 0, 0, 0, 3),
(58, 'Angsle', '', 15, 'http://', 'product_demos_Angsle_1.jpg', '', '', 0, 0, 0, 3),
(59, 'Batagor', '', 15, 'http://', 'product_demos_Batagor_1.jpg', '', '', 0, 0, 0, 3),
(60, 'Burger', '', 15, 'http://', 'product_demos_Burger_1.jpg', '', '', 0, 0, 0, 3),
(61, 'Gorengan', '', 15, 'http://', 'product_demos_Gorengan_1.png', '', '', 0, 0, 0, 3),
(62, 'Semur Jengkol', '', 15, 'http://', 'product_demos_Semur_Jengkol_1.jpg', '', '', 0, 0, 0, 3),
(63, 'Kebab', '', 15, 'http://', 'product_demos_Kebab_1.jpg', '', '', 0, 0, 0, 3),
(64, 'Kentang Goreng', '', 15, 'http://', 'product_demos_Kentang_Goreng_1.jpg', '', '', 0, 0, 0, 3),
(65, 'Seblak', '', 15, 'http://', 'product_demos_Seblak_1.jpg', '', '', 0, 0, 0, 3),
(66, 'Siomay', '', 15, 'http://', 'product_demos_Siomay_1.jpg', '', '', 0, 0, 0, 3),
(67, 'Terang Bulan', '', 15, 'http://', 'product_demos_Terang_Bulan_1.jpg', '', '', 0, 0, 0, 3),
(68, 'Martabak', '', 15, 'http://', 'product_demos_Martabak_1.jpg', '', '', 0, 0, 0, 3),
(69, 'Pisang Keju', '', 15, 'http://', 'product_demos_Pisang_Keju_1.jpg', '', '', 0, 0, 0, 3),
(70, 'Jamu', '', 14, 'http://', 'product_demos_Jamu_1.jpg', '', '', 0, 0, 0, 3),
(71, 'Jus Buah', '', 14, 'http://', 'product_demos_Jus_Buah_1.jpg', '', '', 0, 0, 0, 3),
(72, 'Kopi', '', 14, 'http://', 'product_demos_Kopi_1.jpg', '', '', 0, 0, 0, 3),
(73, 'Susu', '', 14, 'http://', 'product_demos_Susu_1.jpg', '', '', 0, 0, 0, 3),
(74, 'Teh', '', 14, 'http://', 'product_demos_Teh_1.jpg', '', '', 0, 0, 0, 3),
(75, 'Cokelat', '', 14, 'http://', 'product_demos_Cokelat_1.jpg', '', '', 0, 0, 0, 3),
(76, 'Vixion', 'yamaga', 2, 'http://yamaha.com', 'product_inagata_Vixion_1.jpg', '', '', 0, 0, 0, 1),
(78, 'Boyfriend Jeans', 'Celana jeans kekinian model boyfriend', 19, 'http://blanja.com', 'product_lavedaarshanda_Boyfriend_Jeans_1.jpeg', '', '', 0, 0, 0, 9),
(79, 'Seblak', 'Makanan khas Bandung', 22, 'http://seblakhuha.com', 'product_lavedaarshanda_Seblak_1.jpg', '', '', 0, 0, 0, 9),
(80, 'The Ordinary', 'Serum yang membuat wajah glowing', 24, 'http://cosmetify.com', 'product_lavedaarshanda_The_Ordinary_1.jpg', '', '', 0, 0, 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tb_profil`
--

CREATE TABLE `tb_profil` (
  `id_akun` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `visi` varchar(100) NOT NULL,
  `misi` varchar(100) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `banner` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profil`
--

INSERT INTO `tb_profil` (`id_akun`, `nama_perusahaan`, `deskripsi`, `visi`, `misi`, `logo`, `alamat`, `telp`, `email`, `instagram`, `facebook`, `youtube`, `banner`) VALUES
(1, 'inagata technosmith', 'ini deskripsi perusahaan', '', '', 'file_Inagata_Technosmith.png', 'Griyashanta L.110 lowokwaru', '0867812979', 'inagata@contact.com', 'https://instagram.com/inagatatechno?utm_source=ig_profile_share&igshid=1dbppx2kkxwx6', 'https://www.facebook.com/inagatatechno/', 'https://www.youtube.com/channel/UCx17brpNulHSMoJBkRlC3Kg', 'banner_Inagata_Technosmith.jpg'),
(2, 'Gihik Store', '', 'Mensejahterakan masyarakat Indonesia', 'Menjual produk dengan harga mahasiswa', 'logo_asroriasrori.jpg', 'Jl. Candi VI D Malang', '082141792401', 'zainurasrori@gmail.com', '', '', '', 'bannerpf_asroriasrori.jpg'),
(3, 'demos', 'Warung makan enak sedia pemesan online', 'Makanan Penyemangat Jiwa', 'Membuat semua orang menikmati makanan dengan senyuman', 'logo_demos.png', 'Jl. Negara Km. 82', '082114833338', 'demos@gmail.com', '', '', '', 'bannerpf_demos.jpg'),
(4, 'Berlian Gita Cahyani', '', '', '', '', '', '082264487868', 'berliangita.cahyani45@gma', '', '', '', ''),
(5, 'inagata', '', '', '', 'logo_ninno.png', '', '088217262025', 'khsunul20032002@gmail.com', '', '', '', ''),
(6, 'inagata', '', '', '', '', '', '088217262025', 'febrimozak@gmail.com', '', '', '', ''),
(9, 'PT Ina Gata Persada', '', '', '', '', 'Jl. Suhat Malang', '081249122677', 'arshanda12@gmail.com', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_varian`
--

CREATE TABLE `tb_varian` (
  `id_varian` int(11) NOT NULL,
  `varian` varchar(100) NOT NULL,
  `harga_pokok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_voucher`
--

CREATE TABLE `tb_voucher` (
  `id` int(11) NOT NULL,
  `voucher` varchar(100) NOT NULL,
  `id_akun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_voucher`
--

INSERT INTO `tb_voucher` (`id`, `voucher`, `id_akun`) VALUES
(6, 'Cashback', 9),
(7, 'Gratis Ongkir', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akun`
--
ALTER TABLE `tb_akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `tb_banner`
--
ALTER TABLE `tb_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detail_keranjang`
--
ALTER TABLE `tb_detail_keranjang`
  ADD PRIMARY KEY (`id_detail_keranjang`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_keranjang`
--
ALTER TABLE `tb_keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profil`
--
ALTER TABLE `tb_profil`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_varian`
--
ALTER TABLE `tb_varian`
  ADD PRIMARY KEY (`id_varian`);

--
-- Indexes for table `tb_voucher`
--
ALTER TABLE `tb_voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_akun`
--
ALTER TABLE `tb_akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_detail_keranjang`
--
ALTER TABLE `tb_detail_keranjang`
  MODIFY `id_detail_keranjang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tb_keranjang`
--
ALTER TABLE `tb_keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `tb_profil`
--
ALTER TABLE `tb_profil`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_varian`
--
ALTER TABLE `tb_varian`
  MODIFY `id_varian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_voucher`
--
ALTER TABLE `tb_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
